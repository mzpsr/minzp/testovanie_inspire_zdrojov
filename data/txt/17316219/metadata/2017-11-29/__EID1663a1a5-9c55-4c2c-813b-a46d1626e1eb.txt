29.11.2017 21:32:16 - Preparing Test Run metadata_17316219_Geodetick&yacute; a kartografick&yacute; &uacute;stav Bratislava (initiated Wed Nov 29 21:32:16 CET 2017)
29.11.2017 21:32:16 - Resolving Executable Test Suite dependencies
29.11.2017 21:32:16 - Preparing 2 Test Task:
29.11.2017 21:32:16 -  TestTask 1 (7ccf1f9a-03fd-4522-af53-5d61d501ec65)
29.11.2017 21:32:16 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'LAZY.e3500038-e37c-4dcf-806c-6bc82d585b3b'
29.11.2017 21:32:16 -  with parameters: 
29.11.2017 21:32:16 - etf.testcases = *
29.11.2017 21:32:16 -  TestTask 2 (a5acc46c-8ef4-40c3-94ac-fe9787200dd3)
29.11.2017 21:32:16 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119 (EID: ec7323d5-d8f0-4cfe-b23a-b826df86d58c, V: 0.2.4 )'
29.11.2017 21:32:16 -  with parameters: 
29.11.2017 21:32:16 - etf.testcases = *
29.11.2017 21:32:16 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
29.11.2017 21:32:16 - Setting state to CREATED
29.11.2017 21:32:16 - Changed state from CREATED to INITIALIZING
29.11.2017 21:32:16 - Starting TestRun.1663a1a5-9c55-4c2c-813b-a46d1626e1eb at 2017-11-29T21:32:17+01:00
29.11.2017 21:32:17 - Changed state from INITIALIZING to INITIALIZED
29.11.2017 21:32:17 - TestRunTask initialized
29.11.2017 21:32:17 - Creating new tests databases to speed up tests.
29.11.2017 21:32:17 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
29.11.2017 21:32:17 - Optimizing last database etf-tdb-ac85bd73-ef3f-47e7-9294-f4fc2155d93d-0 
29.11.2017 21:32:17 - Import completed
29.11.2017 21:32:18 - Validation ended with 0 error(s)
29.11.2017 21:32:18 - Compiling test script
29.11.2017 21:32:18 - Starting XQuery tests
29.11.2017 21:32:18 - "Testing 1 records"
29.11.2017 21:32:18 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/xml/ets-md-xml-bsxets.xml"
29.11.2017 21:32:18 - "Statistics table: 0 ms"
29.11.2017 21:32:18 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' started"
29.11.2017 21:32:18 - "Test Case 'Schema validation' started"
29.11.2017 21:32:20 - "Validating file GetRecordByIdResponse.xml: 2014 ms"
29.11.2017 21:32:20 - "Test Assertion 'md-xml.a.1: Validate XML documents': PASSED - 2014 ms"
29.11.2017 21:32:20 - "Test Case 'Schema validation' finished: PASSED"
29.11.2017 21:32:20 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' finished: PASSED"
29.11.2017 21:32:20 - Releasing resources
29.11.2017 21:32:20 - TestRunTask initialized
29.11.2017 21:32:20 - Recreating new tests databases as the Test Object has changed!
29.11.2017 21:32:20 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
29.11.2017 21:32:20 - Optimizing last database etf-tdb-ac85bd73-ef3f-47e7-9294-f4fc2155d93d-0 
29.11.2017 21:32:20 - Import completed
29.11.2017 21:32:20 - Validation ended with 0 error(s)
29.11.2017 21:32:20 - Compiling test script
29.11.2017 21:32:20 - Starting XQuery tests
29.11.2017 21:32:20 - "Testing 1 records"
29.11.2017 21:32:20 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/iso/ets-md-iso-bsxets.xml"
29.11.2017 21:32:20 - "Statistics table: 0 ms"
29.11.2017 21:32:20 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' started"
29.11.2017 21:32:20 - "Test Case 'Common tests' started"
29.11.2017 21:32:20 - "Test Assertion 'md-iso.a.1: Title': PASSED - 0 ms"
29.11.2017 21:32:20 - "Test Assertion 'md-iso.a.2: Abstract': PASSED - 1 ms"
29.11.2017 21:32:20 - "Test Assertion 'md-iso.a.3: Access and use conditions': PASSED_MANUAL - 0 ms"
29.11.2017 21:32:20 - "Test Assertion 'md-iso.a.4: Public access': PASSED - 0 ms"
29.11.2017 21:32:20 - "Test Assertion 'md-iso.a.5: Specification': PASSED - 0 ms"
29.11.2017 21:32:20 - "Test Assertion 'md-iso.a.6: Language': PASSED - 0 ms"
29.11.2017 21:32:20 - "Test Assertion 'md-iso.a.7: Metadata contact': PASSED - 0 ms"
29.11.2017 21:32:20 - "Test Assertion 'md-iso.a.8: Metadata contact role': PASSED - 0 ms"
29.11.2017 21:32:20 - "Test Assertion 'md-iso.a.9: Resource creation date': PASSED - 0 ms"
29.11.2017 21:32:20 - "Test Assertion 'md-iso.a.10: Responsible party contact info': PASSED - 0 ms"
29.11.2017 21:32:20 - "Test Assertion 'md-iso.a.11: Responsible party role': PASSED - 1 ms"
29.11.2017 21:32:20 - "Test Case 'Common tests' finished: PASSED_MANUAL"
29.11.2017 21:32:20 - "Test Case 'Hierarchy level' started"
29.11.2017 21:32:20 - "Test Assertion 'md-iso.b.1: Hierarchy': PASSED - 0 ms"
29.11.2017 21:32:20 - "Test Case 'Hierarchy level' finished: PASSED"
29.11.2017 21:32:20 - "Test Case 'Dataset (series) tests' started"
29.11.2017 21:32:20 - "Test Assertion 'md-iso.c.1: Dataset identification': PASSED - 0 ms"
29.11.2017 21:32:20 - "Test Assertion 'md-iso.c.2: Dataset language': PASSED - 0 ms"
29.11.2017 21:32:20 - "Checking URL: 'http://www.geoportal.sk'"
29.11.2017 21:32:21 - "Checking URL: 'https://www.geoportal.sk/'"
29.11.2017 21:32:21 - "Exception: sun.security.validator.ValidatorException: PKIX path building failed: sun.security.provider.certpath.SunCertPathBuilderException: unable to find valid certification path to requested target URL: https://www.geoportal.sk/"
29.11.2017 21:32:21 - "Test Assertion 'md-iso.c.3: Dataset linkage': FAILED - 270 ms"
29.11.2017 21:32:21 - "Test Assertion 'md-iso.c.4: Dataset conformity': PASSED - 0 ms"
29.11.2017 21:32:21 - "Test Assertion 'md-iso.c.5: Dataset topic': PASSED - 0 ms"
29.11.2017 21:32:21 - "Test Assertion 'md-iso.c.6: Dataset geographic Bounding box': PASSED_MANUAL - 0 ms"
29.11.2017 21:32:21 - "Test Assertion 'md-iso.c.7: Dataset lineage': PASSED - 0 ms"
29.11.2017 21:32:21 - "Test Case 'Dataset (series) tests' finished: FAILED"
29.11.2017 21:32:21 - "Test Case 'Service tests' started"
29.11.2017 21:32:21 - "Test Assertion 'md-iso.d.1: Service type': PASSED - 0 ms"
29.11.2017 21:32:21 - "Test Assertion 'md-iso.d.2: Service linkage': PASSED - 0 ms"
29.11.2017 21:32:21 - "Test Assertion 'md-iso.d.3: Coupled resource': PASSED - 0 ms"
29.11.2017 21:32:21 - "Test Case 'Service tests' finished: PASSED"
29.11.2017 21:32:21 - "Test Case 'Keywords' started"
29.11.2017 21:32:21 - "Test Assertion 'md-iso.e.1: Keywords': PASSED - 1 ms"
29.11.2017 21:32:21 - "Test Case 'Keywords' finished: PASSED"
29.11.2017 21:32:21 - "Test Case 'Keywords - details' started"
29.11.2017 21:32:21 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.bg.atom'"
29.11.2017 21:32:21 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.cs.atom'"
29.11.2017 21:32:21 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.da.atom'"
29.11.2017 21:32:21 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.de.atom'"
29.11.2017 21:32:21 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.et.atom'"
29.11.2017 21:32:21 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.el.atom'"
29.11.2017 21:32:21 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.en.atom'"
29.11.2017 21:32:21 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.es.atom'"
29.11.2017 21:32:21 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.fr.atom'"
29.11.2017 21:32:21 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.hr.atom'"
29.11.2017 21:32:21 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.it.atom'"
29.11.2017 21:32:21 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.lv.atom'"
29.11.2017 21:32:21 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.lt.atom'"
29.11.2017 21:32:21 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.hu.atom'"
29.11.2017 21:32:21 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.mt.atom'"
29.11.2017 21:32:21 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.nl.atom'"
29.11.2017 21:32:21 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.pl.atom'"
29.11.2017 21:32:21 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.pt.atom'"
29.11.2017 21:32:21 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.ro.atom'"
29.11.2017 21:32:21 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.sk.atom'"
29.11.2017 21:32:21 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.sl.atom'"
29.11.2017 21:32:21 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.fi.atom'"
29.11.2017 21:32:21 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.sv.atom'"
29.11.2017 21:32:21 - "Checking URL: 'http://inspire.ec.europa.eu/theme/theme.en.atom'"
29.11.2017 21:32:21 - "Test Assertion 'md-iso.f.1: Dataset keyword': PASSED - 416 ms"
29.11.2017 21:32:21 - "Test Assertion 'md-iso.f.2: Service keyword': PASSED - 0 ms"
29.11.2017 21:32:21 - "Test Assertion 'md-iso.f.3: Keywords in vocabulary grouped': PASSED - 0 ms"
29.11.2017 21:32:21 - "Test Assertion 'md-iso.f.4: Vocabulary information': PASSED - 0 ms"
29.11.2017 21:32:21 - "Test Case 'Keywords - details' finished: PASSED"
29.11.2017 21:32:21 - "Test Case 'Temporal extent' started"
29.11.2017 21:32:21 - "Test Assertion 'md-iso.g.1: Temporal extent': PASSED - 0 ms"
29.11.2017 21:32:21 - "Test Case 'Temporal extent' finished: PASSED"
29.11.2017 21:32:21 - "Test Case 'Temporal extent - details' started"
29.11.2017 21:32:21 - "Test Assertion 'md-iso.h.1: Temporal date': PASSED - 0 ms"
29.11.2017 21:32:21 - "Test Case 'Temporal extent - details' finished: PASSED"
29.11.2017 21:32:21 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' finished: FAILED"
29.11.2017 21:32:22 - Releasing resources
29.11.2017 21:32:22 - Changed state from INITIALIZED to RUNNING
29.11.2017 21:32:22 - Duration: 6sec
29.11.2017 21:32:22 - TestRun finished
29.11.2017 21:32:22 - Changed state from RUNNING to COMPLETED
