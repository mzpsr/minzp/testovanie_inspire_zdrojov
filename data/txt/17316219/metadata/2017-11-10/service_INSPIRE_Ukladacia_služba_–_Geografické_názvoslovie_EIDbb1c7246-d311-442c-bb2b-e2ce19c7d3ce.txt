10.11.2017 17:58:02 - Preparing Test Run metadata (initiated Fri Nov 10 17:58:02 CET 2017)
10.11.2017 17:58:02 - Resolving Executable Test Suite dependencies
10.11.2017 17:58:02 - Preparing 2 Test Task:
10.11.2017 17:58:02 -  TestTask 1 (dac943a8-cdf0-411c-987a-ed7d2451a439)
10.11.2017 17:58:02 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'LAZY.e3500038-e37c-4dcf-806c-6bc82d585b3b'
10.11.2017 17:58:02 -  with parameters: 
10.11.2017 17:58:02 - etf.testcases = *
10.11.2017 17:58:02 -  TestTask 2 (7f4dd3ce-87d5-4f4a-b443-837a0dbd8358)
10.11.2017 17:58:02 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119 (EID: ec7323d5-d8f0-4cfe-b23a-b826df86d58c, V: 0.2.4 )'
10.11.2017 17:58:02 -  with parameters: 
10.11.2017 17:58:02 - etf.testcases = *
10.11.2017 17:58:02 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
10.11.2017 17:58:02 - Setting state to CREATED
10.11.2017 17:58:02 - Changed state from CREATED to INITIALIZING
10.11.2017 17:58:03 - Starting TestRun.bb1c7246-d311-442c-bb2b-e2ce19c7d3ce at 2017-11-10T17:58:04+01:00
10.11.2017 17:58:04 - Changed state from INITIALIZING to INITIALIZED
10.11.2017 17:58:04 - TestRunTask initialized
10.11.2017 17:58:04 - Creating new tests databases to speed up tests.
10.11.2017 17:58:04 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
10.11.2017 17:58:04 - Optimizing last database etf-tdb-e97fca1d-369a-4406-9aca-0a142d25b956-0 
10.11.2017 17:58:04 - Import completed
10.11.2017 17:58:04 - Validation ended with 0 error(s)
10.11.2017 17:58:04 - Compiling test script
10.11.2017 17:58:04 - Starting XQuery tests
10.11.2017 17:58:04 - "Testing 1 records"
10.11.2017 17:58:04 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/xml/ets-md-xml-bsxets.xml"
10.11.2017 17:58:04 - "Statistics table: 0 ms"
10.11.2017 17:58:04 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' started"
10.11.2017 17:58:04 - "Test Case 'Schema validation' started"
10.11.2017 17:58:07 - "Validating file GetRecordByIdResponse.xml: 2249 ms"
10.11.2017 17:58:07 - "Test Assertion 'md-xml.a.1: Validate XML documents': PASSED - 2249 ms"
10.11.2017 17:58:07 - "Test Case 'Schema validation' finished: PASSED"
10.11.2017 17:58:07 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' finished: PASSED"
10.11.2017 17:58:07 - Releasing resources
10.11.2017 17:58:07 - TestRunTask initialized
10.11.2017 17:58:07 - Recreating new tests databases as the Test Object has changed!
10.11.2017 17:58:07 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
10.11.2017 17:58:07 - Optimizing last database etf-tdb-e97fca1d-369a-4406-9aca-0a142d25b956-0 
10.11.2017 17:58:07 - Import completed
10.11.2017 17:58:07 - Validation ended with 0 error(s)
10.11.2017 17:58:07 - Compiling test script
10.11.2017 17:58:07 - Starting XQuery tests
10.11.2017 17:58:07 - "Testing 1 records"
10.11.2017 17:58:07 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/iso/ets-md-iso-bsxets.xml"
10.11.2017 17:58:07 - "Statistics table: 0 ms"
10.11.2017 17:58:07 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' started"
10.11.2017 17:58:07 - "Test Case 'Common tests' started"
10.11.2017 17:58:07 - "Test Assertion 'md-iso.a.1: Title': PASSED - 0 ms"
10.11.2017 17:58:07 - "Test Assertion 'md-iso.a.2: Abstract': PASSED - 0 ms"
10.11.2017 17:58:07 - "Test Assertion 'md-iso.a.3: Access and use conditions': PASSED_MANUAL - 0 ms"
10.11.2017 17:58:07 - "Test Assertion 'md-iso.a.4: Public access': PASSED - 0 ms"
10.11.2017 17:58:07 - "Test Assertion 'md-iso.a.5: Specification': PASSED - 0 ms"
10.11.2017 17:58:07 - "Test Assertion 'md-iso.a.6: Language': PASSED - 0 ms"
10.11.2017 17:58:07 - "Test Assertion 'md-iso.a.7: Metadata contact': PASSED - 0 ms"
10.11.2017 17:58:07 - "Test Assertion 'md-iso.a.8: Metadata contact role': PASSED - 0 ms"
10.11.2017 17:58:07 - "Test Assertion 'md-iso.a.9: Resource creation date': PASSED - 0 ms"
10.11.2017 17:58:07 - "Test Assertion 'md-iso.a.10: Responsible party contact info': PASSED - 0 ms"
10.11.2017 17:58:07 - "Test Assertion 'md-iso.a.11: Responsible party role': PASSED - 0 ms"
10.11.2017 17:58:07 - "Test Case 'Common tests' finished: PASSED_MANUAL"
10.11.2017 17:58:07 - "Test Case 'Hierarchy level' started"
10.11.2017 17:58:07 - "Test Assertion 'md-iso.b.1: Hierarchy': PASSED - 0 ms"
10.11.2017 17:58:07 - "Test Case 'Hierarchy level' finished: PASSED"
10.11.2017 17:58:07 - "Test Case 'Dataset (series) tests' started"
10.11.2017 17:58:07 - "Test Assertion 'md-iso.c.1: Dataset identification': PASSED - 0 ms"
10.11.2017 17:58:07 - "Test Assertion 'md-iso.c.2: Dataset language': PASSED - 0 ms"
10.11.2017 17:58:07 - "Test Assertion 'md-iso.c.3: Dataset linkage': PASSED - 0 ms"
10.11.2017 17:58:07 - "Test Assertion 'md-iso.c.4: Dataset conformity': PASSED - 0 ms"
10.11.2017 17:58:07 - "Test Assertion 'md-iso.c.5: Dataset topic': PASSED - 0 ms"
10.11.2017 17:58:07 - "Test Assertion 'md-iso.c.6: Dataset geographic Bounding box': PASSED - 0 ms"
10.11.2017 17:58:07 - "Test Assertion 'md-iso.c.7: Dataset lineage': PASSED - 0 ms"
10.11.2017 17:58:07 - "Test Case 'Dataset (series) tests' finished: PASSED"
10.11.2017 17:58:07 - "Test Case 'Service tests' started"
10.11.2017 17:58:07 - "Test Assertion 'md-iso.d.1: Service type': PASSED - 0 ms"
10.11.2017 17:58:07 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_geographical_names_wfs/service.svc/get'"
10.11.2017 17:58:08 - "Test Assertion 'md-iso.d.2: Service linkage': PASSED_MANUAL - 197 ms"
10.11.2017 17:58:08 - "Checking URL: 'https://zbgisws.skgeodesy.sk/zbgiscsw/service.svc/get?REQUEST=GetRecordById&amp;SERVICE=CSW&amp;VERSION=2.0.2&amp;OUTPUTSCHEMA=http://www.isotc211.org/2005/gmd&amp;ELEMENTSETNAME=full&amp;Id=https://data.gov.sk/set/rpi/gmd/17316219/SK_UGKK_ZBGIS_INSPIRE_GN'"
10.11.2017 17:58:08 - "Test Assertion 'md-iso.d.3: Coupled resource': PASSED - 662 ms"
10.11.2017 17:58:08 - "Test Case 'Service tests' finished: PASSED_MANUAL"
10.11.2017 17:58:08 - "Test Case 'Keywords' started"
10.11.2017 17:58:08 - "Test Assertion 'md-iso.e.1: Keywords': PASSED - 0 ms"
10.11.2017 17:58:08 - "Test Case 'Keywords' finished: PASSED"
10.11.2017 17:58:08 - "Test Case 'Keywords - details' started"
10.11.2017 17:58:08 - "Test Assertion 'md-iso.f.1: Dataset keyword': PASSED - 0 ms"
10.11.2017 17:58:08 - "Checking URL: 'http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/SpatialDataServiceCategory.en.atom'"
10.11.2017 17:58:08 - "Test Assertion 'md-iso.f.2: Service keyword': PASSED - 5 ms"
10.11.2017 17:58:08 - "Test Assertion 'md-iso.f.3: Keywords in vocabulary grouped': PASSED - 1 ms"
10.11.2017 17:58:08 - "Test Assertion 'md-iso.f.4: Vocabulary information': PASSED - 0 ms"
10.11.2017 17:58:08 - "Test Case 'Keywords - details' finished: PASSED"
10.11.2017 17:58:08 - "Test Case 'Temporal extent' started"
10.11.2017 17:58:08 - "Test Assertion 'md-iso.g.1: Temporal extent': PASSED - 0 ms"
10.11.2017 17:58:08 - "Test Case 'Temporal extent' finished: PASSED"
10.11.2017 17:58:08 - "Test Case 'Temporal extent - details' started"
10.11.2017 17:58:08 - "Test Assertion 'md-iso.h.1: Temporal date': PASSED - 0 ms"
10.11.2017 17:58:08 - "Test Case 'Temporal extent - details' finished: PASSED"
10.11.2017 17:58:08 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' finished: PASSED_MANUAL"
10.11.2017 17:58:09 - Releasing resources
10.11.2017 17:58:09 - Changed state from INITIALIZED to RUNNING
10.11.2017 17:58:10 - Duration: 7sec
10.11.2017 17:58:10 - TestRun finished
10.11.2017 17:58:10 - Changed state from RUNNING to COMPLETED
