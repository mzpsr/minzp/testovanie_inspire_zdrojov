12.12.2017 02:02:11 - Preparing Test Run metadata_17316219_Geodetick&yacute; a kartografick&yacute; &uacute;stav Bratislava(GK&Uacute;)_INSPIRE Ukladacia služba &ndash; Geografick&eacute; n&aacute;zvoslovie (initiated Tue Dec 12 02:02:11 CET 2017)
12.12.2017 02:02:11 - Resolving Executable Test Suite dependencies
12.12.2017 02:02:11 - Preparing 2 Test Task:
12.12.2017 02:02:11 -  TestTask 1 (9709d115-c59b-4dab-803f-69851330ca1e)
12.12.2017 02:02:11 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'LAZY.e3500038-e37c-4dcf-806c-6bc82d585b3b'
12.12.2017 02:02:11 -  with parameters: 
12.12.2017 02:02:11 - etf.testcases = *
12.12.2017 02:02:11 -  TestTask 2 (cb2d4ad3-1072-4e8f-9d43-4e427db7c12f)
12.12.2017 02:02:11 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119 (EID: ec7323d5-d8f0-4cfe-b23a-b826df86d58c, V: 0.2.5 )'
12.12.2017 02:02:11 -  with parameters: 
12.12.2017 02:02:11 - etf.testcases = *
12.12.2017 02:02:11 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
12.12.2017 02:02:11 - Setting state to CREATED
12.12.2017 02:02:11 - Changed state from CREATED to INITIALIZING
12.12.2017 02:02:11 - Starting TestRun.73a31f69-8fa4-4733-8259-c128d58b2a44 at 2017-12-12T02:02:12+01:00
12.12.2017 02:02:12 - Changed state from INITIALIZING to INITIALIZED
12.12.2017 02:02:12 - TestRunTask initialized
12.12.2017 02:02:12 - Creating new tests databases to speed up tests.
12.12.2017 02:02:12 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 02:02:12 - Optimizing last database etf-tdb-479ffa96-fa00-4b26-a960-313a88fbbe88-0 
12.12.2017 02:02:12 - Import completed
12.12.2017 02:02:12 - Validation ended with 0 error(s)
12.12.2017 02:02:12 - Compiling test script
12.12.2017 02:02:12 - Starting XQuery tests
12.12.2017 02:02:13 - "Testing 1 records"
12.12.2017 02:02:13 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/xml/ets-md-xml-bsxets.xml"
12.12.2017 02:02:13 - "Statistics table: 0 ms"
12.12.2017 02:02:13 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' started"
12.12.2017 02:02:13 - "Test Case 'Schema validation' started"
12.12.2017 02:02:15 - "Validating file GetRecordByIdResponse.xml: 2220 ms"
12.12.2017 02:02:15 - "Test Assertion 'md-xml.a.1: Validate XML documents': PASSED - 2221 ms"
12.12.2017 02:02:15 - "Test Case 'Schema validation' finished: PASSED"
12.12.2017 02:02:15 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' finished: PASSED"
12.12.2017 02:02:21 - Releasing resources
12.12.2017 02:02:21 - TestRunTask initialized
12.12.2017 02:02:21 - Recreating new tests databases as the Test Object has changed!
12.12.2017 02:02:21 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.12.2017 02:02:21 - Optimizing last database etf-tdb-479ffa96-fa00-4b26-a960-313a88fbbe88-0 
12.12.2017 02:02:21 - Import completed
12.12.2017 02:02:22 - Validation ended with 0 error(s)
12.12.2017 02:02:22 - Compiling test script
12.12.2017 02:02:22 - Starting XQuery tests
12.12.2017 02:02:22 - "Testing 1 records"
12.12.2017 02:02:22 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/iso/ets-md-iso-bsxets.xml"
12.12.2017 02:02:22 - "Statistics table: 1 ms"
12.12.2017 02:02:22 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' started"
12.12.2017 02:02:22 - "Test Case 'Common tests' started"
12.12.2017 02:02:22 - "Test Assertion 'md-iso.a.1: Title': PASSED - 0 ms"
12.12.2017 02:02:22 - "Test Assertion 'md-iso.a.2: Abstract': PASSED - 1 ms"
12.12.2017 02:02:22 - "Test Assertion 'md-iso.a.3: Access and use conditions': PASSED_MANUAL - 0 ms"
12.12.2017 02:02:22 - "Test Assertion 'md-iso.a.4: Public access': PASSED - 0 ms"
12.12.2017 02:02:22 - "Test Assertion 'md-iso.a.5: Specification': PASSED - 0 ms"
12.12.2017 02:02:22 - "Test Assertion 'md-iso.a.6: Language': PASSED - 0 ms"
12.12.2017 02:02:22 - "Test Assertion 'md-iso.a.7: Metadata contact': PASSED - 1 ms"
12.12.2017 02:02:22 - "Test Assertion 'md-iso.a.8: Metadata contact role': PASSED - 0 ms"
12.12.2017 02:02:22 - "Test Assertion 'md-iso.a.9: Resource creation date': PASSED - 0 ms"
12.12.2017 02:02:22 - "Test Assertion 'md-iso.a.10: Responsible party contact info': PASSED - 0 ms"
12.12.2017 02:02:22 - "Test Assertion 'md-iso.a.11: Responsible party role': PASSED - 0 ms"
12.12.2017 02:02:22 - "Test Case 'Common tests' finished: PASSED_MANUAL"
12.12.2017 02:02:22 - "Test Case 'Hierarchy level' started"
12.12.2017 02:02:22 - "Test Assertion 'md-iso.b.1: Hierarchy': PASSED - 0 ms"
12.12.2017 02:02:22 - "Test Case 'Hierarchy level' finished: PASSED"
12.12.2017 02:02:22 - "Test Case 'Dataset (series) tests' started"
12.12.2017 02:02:22 - "Test Assertion 'md-iso.c.1: Dataset identification': PASSED - 0 ms"
12.12.2017 02:02:22 - "Test Assertion 'md-iso.c.2: Dataset language': PASSED - 0 ms"
12.12.2017 02:02:22 - "Test Assertion 'md-iso.c.3: Dataset linkage': PASSED - 0 ms"
12.12.2017 02:02:22 - "Test Assertion 'md-iso.c.4: Dataset conformity': PASSED - 0 ms"
12.12.2017 02:02:22 - "Test Assertion 'md-iso.c.5: Dataset topic': PASSED - 0 ms"
12.12.2017 02:02:22 - "Test Assertion 'md-iso.c.6: Dataset geographic Bounding box': PASSED - 0 ms"
12.12.2017 02:02:22 - "Test Assertion 'md-iso.c.7: Dataset lineage': PASSED - 0 ms"
12.12.2017 02:02:22 - "Test Case 'Dataset (series) tests' finished: PASSED"
12.12.2017 02:02:22 - "Test Case 'Service tests' started"
12.12.2017 02:02:22 - "Test Assertion 'md-iso.d.1: Service type': PASSED - 0 ms"
12.12.2017 02:02:22 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_geographical_names_wfs/service.svc/get'"
12.12.2017 02:02:22 - "Test Assertion 'md-iso.d.2: Service linkage': PASSED_MANUAL - 161 ms"
12.12.2017 02:02:22 - "Checking URL: 'https://zbgisws.skgeodesy.sk/zbgiscsw/service.svc/get?REQUEST=GetRecordById&amp;SERVICE=CSW&amp;VERSION=2.0.2&amp;OUTPUTSCHEMA=http://www.isotc211.org/2005/gmd&amp;ELEMENTSETNAME=full&amp;Id=https://data.gov.sk/set/rpi/gmd/17316219/SK_UGKK_..."
12.12.2017 02:02:22 - "Test Assertion 'md-iso.d.3: Coupled resource': PASSED - 537 ms"
12.12.2017 02:02:22 - "Test Case 'Service tests' finished: PASSED_MANUAL"
12.12.2017 02:02:22 - "Test Case 'Keywords' started"
12.12.2017 02:02:22 - "Test Assertion 'md-iso.e.1: Keywords': PASSED - 0 ms"
12.12.2017 02:02:22 - "Test Case 'Keywords' finished: PASSED"
12.12.2017 02:02:22 - "Test Case 'Keywords - details' started"
12.12.2017 02:02:22 - "Test Assertion 'md-iso.f.1: Dataset keyword': PASSED - 0 ms"
12.12.2017 02:02:22 - "Checking URL: 'http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/SpatialDataServiceCategory.en.atom'"
12.12.2017 02:02:23 - "Test Assertion 'md-iso.f.2: Service keyword': PASSED - 35 ms"
12.12.2017 02:02:23 - "Test Assertion 'md-iso.f.3: Keywords in vocabulary grouped': PASSED - 0 ms"
12.12.2017 02:02:23 - "Test Assertion 'md-iso.f.4: Vocabulary information': PASSED - 1 ms"
12.12.2017 02:02:23 - "Test Case 'Keywords - details' finished: PASSED"
12.12.2017 02:02:23 - "Test Case 'Temporal extent' started"
12.12.2017 02:02:23 - "Test Assertion 'md-iso.g.1: Temporal extent': PASSED - 1 ms"
12.12.2017 02:02:23 - "Test Case 'Temporal extent' finished: PASSED"
12.12.2017 02:02:23 - "Test Case 'Temporal extent - details' started"
12.12.2017 02:02:23 - "Test Assertion 'md-iso.h.1: Temporal date': PASSED - 1 ms"
12.12.2017 02:02:23 - "Test Case 'Temporal extent - details' finished: PASSED"
12.12.2017 02:02:23 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' finished: PASSED_MANUAL"
12.12.2017 02:02:23 - Releasing resources
12.12.2017 02:02:23 - Changed state from INITIALIZED to RUNNING
12.12.2017 02:02:23 - Duration: 12sec
12.12.2017 02:02:23 - TestRun finished
12.12.2017 02:02:23 - Changed state from RUNNING to COMPLETED
