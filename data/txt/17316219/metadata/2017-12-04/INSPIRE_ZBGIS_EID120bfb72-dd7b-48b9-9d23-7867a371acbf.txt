04.12.2017 12:17:05 - Preparing Test Run metadata_17316219_Geodetick&yacute; a kartografick&yacute; &uacute;stav Bratislava(GK&Uacute;)_INSPIRE ZBGIS (initiated Mon Dec 04 12:17:05 CET 2017)
04.12.2017 12:17:05 - Resolving Executable Test Suite dependencies
04.12.2017 12:17:05 - Preparing 2 Test Task:
04.12.2017 12:17:05 -  TestTask 1 (66ab93c6-280f-48f8-864d-22d8b81cbb31)
04.12.2017 12:17:05 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'LAZY.e3500038-e37c-4dcf-806c-6bc82d585b3b'
04.12.2017 12:17:05 -  with parameters: 
04.12.2017 12:17:05 - etf.testcases = *
04.12.2017 12:17:05 -  TestTask 2 (aefaae9c-ddbc-46b7-84d4-e6f3d703d6d9)
04.12.2017 12:17:05 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119 (EID: ec7323d5-d8f0-4cfe-b23a-b826df86d58c, V: 0.2.4 )'
04.12.2017 12:17:05 -  with parameters: 
04.12.2017 12:17:05 - etf.testcases = *
04.12.2017 12:17:05 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
04.12.2017 12:17:05 - Setting state to CREATED
04.12.2017 12:17:05 - Changed state from CREATED to INITIALIZING
04.12.2017 12:17:05 - Starting TestRun.120bfb72-dd7b-48b9-9d23-7867a371acbf at 2017-12-04T12:17:06+01:00
04.12.2017 12:17:06 - Changed state from INITIALIZING to INITIALIZED
04.12.2017 12:17:06 - TestRunTask initialized
04.12.2017 12:17:06 - Creating new tests databases to speed up tests.
04.12.2017 12:17:06 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
04.12.2017 12:17:06 - Optimizing last database etf-tdb-45582ce2-ed1a-442c-8f99-0420cdf1503e-0 
04.12.2017 12:17:06 - Import completed
04.12.2017 12:17:06 - Validation ended with 0 error(s)
04.12.2017 12:17:06 - Compiling test script
04.12.2017 12:17:06 - Starting XQuery tests
04.12.2017 12:17:07 - "Testing 1 records"
04.12.2017 12:17:07 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/xml/ets-md-xml-bsxets.xml"
04.12.2017 12:17:07 - "Statistics table: 0 ms"
04.12.2017 12:17:07 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' started"
04.12.2017 12:17:07 - "Test Case 'Schema validation' started"
04.12.2017 12:17:09 - "Validating file GetRecordByIdResponse.xml: 2114 ms"
04.12.2017 12:17:09 - "Test Assertion 'md-xml.a.1: Validate XML documents': PASSED - 2115 ms"
04.12.2017 12:17:09 - "Test Case 'Schema validation' finished: PASSED"
04.12.2017 12:17:09 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' finished: PASSED"
04.12.2017 12:17:09 - Releasing resources
04.12.2017 12:17:09 - TestRunTask initialized
04.12.2017 12:17:09 - Recreating new tests databases as the Test Object has changed!
04.12.2017 12:17:09 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
04.12.2017 12:17:09 - Optimizing last database etf-tdb-45582ce2-ed1a-442c-8f99-0420cdf1503e-0 
04.12.2017 12:17:09 - Import completed
04.12.2017 12:17:09 - Validation ended with 0 error(s)
04.12.2017 12:17:09 - Compiling test script
04.12.2017 12:17:09 - Starting XQuery tests
04.12.2017 12:17:09 - "Testing 1 records"
04.12.2017 12:17:09 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/iso/ets-md-iso-bsxets.xml"
04.12.2017 12:17:09 - "Statistics table: 0 ms"
04.12.2017 12:17:09 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' started"
04.12.2017 12:17:09 - "Test Case 'Common tests' started"
04.12.2017 12:17:09 - "Test Assertion 'md-iso.a.1: Title': PASSED - 0 ms"
04.12.2017 12:17:09 - "Test Assertion 'md-iso.a.2: Abstract': PASSED - 0 ms"
04.12.2017 12:17:09 - "Test Assertion 'md-iso.a.3: Access and use conditions': PASSED_MANUAL - 0 ms"
04.12.2017 12:17:09 - "Test Assertion 'md-iso.a.4: Public access': PASSED - 0 ms"
04.12.2017 12:17:09 - "Test Assertion 'md-iso.a.5: Specification': PASSED - 0 ms"
04.12.2017 12:17:09 - "Test Assertion 'md-iso.a.6: Language': PASSED - 0 ms"
04.12.2017 12:17:09 - "Test Assertion 'md-iso.a.7: Metadata contact': PASSED - 0 ms"
04.12.2017 12:17:09 - "Test Assertion 'md-iso.a.8: Metadata contact role': PASSED - 0 ms"
04.12.2017 12:17:09 - "Test Assertion 'md-iso.a.9: Resource creation date': PASSED - 0 ms"
04.12.2017 12:17:09 - "Test Assertion 'md-iso.a.10: Responsible party contact info': PASSED - 0 ms"
04.12.2017 12:17:09 - "Test Assertion 'md-iso.a.11: Responsible party role': PASSED - 1 ms"
04.12.2017 12:17:09 - "Test Case 'Common tests' finished: PASSED_MANUAL"
04.12.2017 12:17:09 - "Test Case 'Hierarchy level' started"
04.12.2017 12:17:09 - "Test Assertion 'md-iso.b.1: Hierarchy': PASSED - 0 ms"
04.12.2017 12:17:09 - "Test Case 'Hierarchy level' finished: PASSED"
04.12.2017 12:17:09 - "Test Case 'Dataset (series) tests' started"
04.12.2017 12:17:09 - "Test Assertion 'md-iso.c.1: Dataset identification': PASSED - 1 ms"
04.12.2017 12:17:09 - "Test Assertion 'md-iso.c.2: Dataset language': PASSED - 0 ms"
04.12.2017 12:17:09 - "Test Assertion 'md-iso.c.3: Dataset linkage': PASSED - 0 ms"
04.12.2017 12:17:09 - "Test Assertion 'md-iso.c.4: Dataset conformity': PASSED - 0 ms"
04.12.2017 12:17:09 - "Test Assertion 'md-iso.c.5: Dataset topic': PASSED - 0 ms"
04.12.2017 12:17:09 - "Test Assertion 'md-iso.c.6: Dataset geographic Bounding box': PASSED - 0 ms"
04.12.2017 12:17:09 - "Test Assertion 'md-iso.c.7: Dataset lineage': PASSED - 0 ms"
04.12.2017 12:17:09 - "Test Case 'Dataset (series) tests' finished: PASSED"
04.12.2017 12:17:09 - "Test Case 'Service tests' started"
04.12.2017 12:17:09 - "Test Assertion 'md-iso.d.1: Service type': PASSED - 0 ms"
04.12.2017 12:17:09 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get'"
04.12.2017 12:17:10 - "Test Assertion 'md-iso.d.2: Service linkage': PASSED_MANUAL - 147 ms"
04.12.2017 12:17:10 - "Checking URL: 'https://zbgisws.skgeodesy.sk/zbgiscsw/service.svc/get?REQUEST=GetRecordById&amp;SERVICE=CSW&amp;VERSION=2.0.2&amp;OUTPUTSCHEMA=http://www.isotc211.org/2005/gmd&amp;ELEMENTSETNAME=full&amp;Id=https://data.gov.sk/set/rpi/gmd/17316219/SK_UGKK_INSPIRE_WFS_AU'"
04.12.2017 12:17:10 - "Test Assertion 'md-iso.d.3: Coupled resource': FAILED - 313 ms"
04.12.2017 12:17:10 - "Test Case 'Service tests' finished: FAILED"
04.12.2017 12:17:10 - "Test Case 'Keywords' started"
04.12.2017 12:17:10 - "Test Assertion 'md-iso.e.1: Keywords': PASSED - 0 ms"
04.12.2017 12:17:10 - "Test Case 'Keywords' finished: PASSED"
04.12.2017 12:17:10 - "Test Case 'Keywords - details' started"
04.12.2017 12:17:10 - "Test Assertion 'md-iso.f.1: Dataset keyword': PASSED - 0 ms"
04.12.2017 12:17:10 - "Checking URL: 'http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/SpatialDataServiceCategory.en.atom'"
04.12.2017 12:17:10 - "Test Assertion 'md-iso.f.2: Service keyword': PASSED - 12 ms"
04.12.2017 12:17:10 - "Test Assertion 'md-iso.f.3: Keywords in vocabulary grouped': PASSED - 0 ms"
04.12.2017 12:17:10 - "Test Assertion 'md-iso.f.4: Vocabulary information': PASSED - 0 ms"
04.12.2017 12:17:10 - "Test Case 'Keywords - details' finished: PASSED"
04.12.2017 12:17:10 - "Test Case 'Temporal extent' started"
04.12.2017 12:17:10 - "Test Assertion 'md-iso.g.1: Temporal extent': PASSED - 0 ms"
04.12.2017 12:17:10 - "Test Case 'Temporal extent' finished: PASSED"
04.12.2017 12:17:10 - "Test Case 'Temporal extent - details' started"
04.12.2017 12:17:10 - "Test Assertion 'md-iso.h.1: Temporal date': PASSED - 0 ms"
04.12.2017 12:17:10 - "Test Case 'Temporal extent - details' finished: PASSED"
04.12.2017 12:17:10 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' finished: FAILED"
04.12.2017 12:17:10 - Releasing resources
04.12.2017 12:17:10 - Changed state from INITIALIZED to RUNNING
04.12.2017 12:17:10 - Duration: 5sec
04.12.2017 12:17:10 - TestRun finished
04.12.2017 12:17:10 - Changed state from RUNNING to COMPLETED
