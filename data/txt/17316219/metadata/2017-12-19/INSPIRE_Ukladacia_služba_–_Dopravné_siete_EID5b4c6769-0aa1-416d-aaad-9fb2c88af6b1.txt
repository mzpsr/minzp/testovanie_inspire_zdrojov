19.12.2017 22:57:14 - Preparing Test Run metadata_17316219_Geodetick&yacute; a kartografick&yacute; &uacute;stav Bratislava_INSPIRE Ukladacia služba &ndash; Dopravn&eacute; siete (initiated Tue Dec 19 22:57:14 CET 2017)
19.12.2017 22:57:14 - Resolving Executable Test Suite dependencies
19.12.2017 22:57:14 - Preparing 2 Test Task:
19.12.2017 22:57:14 -  TestTask 1 (9b44e5f7-245a-420a-9e97-962d9430e728)
19.12.2017 22:57:14 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'LAZY.e3500038-e37c-4dcf-806c-6bc82d585b3b'
19.12.2017 22:57:14 -  with parameters: 
19.12.2017 22:57:14 - etf.testcases = *
19.12.2017 22:57:14 -  TestTask 2 (340eb56b-f735-4998-8e93-aceb075c0a48)
19.12.2017 22:57:14 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119 (EID: ec7323d5-d8f0-4cfe-b23a-b826df86d58c, V: 0.2.5 )'
19.12.2017 22:57:14 -  with parameters: 
19.12.2017 22:57:14 - etf.testcases = *
19.12.2017 22:57:14 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
19.12.2017 22:57:14 - Setting state to CREATED
19.12.2017 22:57:14 - Changed state from CREATED to INITIALIZING
19.12.2017 22:57:14 - Starting TestRun.5b4c6769-0aa1-416d-aaad-9fb2c88af6b1 at 2017-12-19T22:57:15+01:00
19.12.2017 22:57:15 - Changed state from INITIALIZING to INITIALIZED
19.12.2017 22:57:15 - TestRunTask initialized
19.12.2017 22:57:15 - Creating new tests databases to speed up tests.
19.12.2017 22:57:15 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
19.12.2017 22:57:15 - Optimizing last database etf-tdb-2b79f3f4-a8d4-4068-958f-bfff3e6c8791-0 
19.12.2017 22:57:15 - Import completed
19.12.2017 22:57:16 - Validation ended with 0 error(s)
19.12.2017 22:57:16 - Compiling test script
19.12.2017 22:57:16 - Starting XQuery tests
19.12.2017 22:57:16 - "Testing 1 records"
19.12.2017 22:57:16 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/xml/ets-md-xml-bsxets.xml"
19.12.2017 22:57:16 - "Statistics table: 0 ms"
19.12.2017 22:57:16 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' started"
19.12.2017 22:57:16 - "Test Case 'Schema validation' started"
19.12.2017 22:57:17 - "Validating file GetRecordByIdResponse.xml: 1457 ms"
19.12.2017 22:57:17 - "Test Assertion 'md-xml.a.1: Validate XML documents': PASSED - 1458 ms"
19.12.2017 22:57:17 - "Test Case 'Schema validation' finished: PASSED"
19.12.2017 22:57:17 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' finished: PASSED"
19.12.2017 22:57:17 - Releasing resources
19.12.2017 22:57:17 - TestRunTask initialized
19.12.2017 22:57:17 - Recreating new tests databases as the Test Object has changed!
19.12.2017 22:57:17 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
19.12.2017 22:57:17 - Optimizing last database etf-tdb-2b79f3f4-a8d4-4068-958f-bfff3e6c8791-0 
19.12.2017 22:57:17 - Import completed
19.12.2017 22:57:18 - Validation ended with 0 error(s)
19.12.2017 22:57:18 - Compiling test script
19.12.2017 22:57:18 - Starting XQuery tests
19.12.2017 22:57:18 - "Testing 1 records"
19.12.2017 22:57:18 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/iso/ets-md-iso-bsxets.xml"
19.12.2017 22:57:18 - "Statistics table: 1 ms"
19.12.2017 22:57:18 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' started"
19.12.2017 22:57:18 - "Test Case 'Common tests' started"
19.12.2017 22:57:18 - "Test Assertion 'md-iso.a.1: Title': PASSED - 0 ms"
19.12.2017 22:57:18 - "Test Assertion 'md-iso.a.2: Abstract': PASSED - 0 ms"
19.12.2017 22:57:18 - "Test Assertion 'md-iso.a.3: Access and use conditions': PASSED_MANUAL - 0 ms"
19.12.2017 22:57:18 - "Test Assertion 'md-iso.a.4: Public access': PASSED - 0 ms"
19.12.2017 22:57:18 - "Test Assertion 'md-iso.a.5: Specification': PASSED - 0 ms"
19.12.2017 22:57:18 - "Test Assertion 'md-iso.a.6: Language': PASSED - 0 ms"
19.12.2017 22:57:18 - "Test Assertion 'md-iso.a.7: Metadata contact': PASSED - 0 ms"
19.12.2017 22:57:18 - "Test Assertion 'md-iso.a.8: Metadata contact role': PASSED - 0 ms"
19.12.2017 22:57:18 - "Test Assertion 'md-iso.a.9: Resource creation date': PASSED - 0 ms"
19.12.2017 22:57:18 - "Test Assertion 'md-iso.a.10: Responsible party contact info': PASSED - 0 ms"
19.12.2017 22:57:18 - "Test Assertion 'md-iso.a.11: Responsible party role': PASSED - 0 ms"
19.12.2017 22:57:18 - "Test Case 'Common tests' finished: PASSED_MANUAL"
19.12.2017 22:57:18 - "Test Case 'Hierarchy level' started"
19.12.2017 22:57:18 - "Test Assertion 'md-iso.b.1: Hierarchy': PASSED - 0 ms"
19.12.2017 22:57:18 - "Test Case 'Hierarchy level' finished: PASSED"
19.12.2017 22:57:18 - "Test Case 'Dataset (series) tests' started"
19.12.2017 22:57:18 - "Test Assertion 'md-iso.c.1: Dataset identification': PASSED - 0 ms"
19.12.2017 22:57:18 - "Test Assertion 'md-iso.c.2: Dataset language': PASSED - 0 ms"
19.12.2017 22:57:18 - "Test Assertion 'md-iso.c.3: Dataset linkage': PASSED - 0 ms"
19.12.2017 22:57:18 - "Test Assertion 'md-iso.c.4: Dataset conformity': PASSED - 1 ms"
19.12.2017 22:57:18 - "Test Assertion 'md-iso.c.5: Dataset topic': PASSED - 0 ms"
19.12.2017 22:57:18 - "Test Assertion 'md-iso.c.6: Dataset geographic Bounding box': PASSED - 0 ms"
19.12.2017 22:57:18 - "Test Assertion 'md-iso.c.7: Dataset lineage': PASSED - 0 ms"
19.12.2017 22:57:18 - "Test Case 'Dataset (series) tests' finished: PASSED"
19.12.2017 22:57:18 - "Test Case 'Service tests' started"
19.12.2017 22:57:18 - "Test Assertion 'md-iso.d.1: Service type': PASSED - 0 ms"
19.12.2017 22:57:18 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_transport_networks_wfs/service.svc/get?'"
19.12.2017 22:57:18 - "Test Assertion 'md-iso.d.2: Service linkage': PASSED_MANUAL - 299 ms"
19.12.2017 22:57:18 - "Checking URL: 'https://zbgisws.skgeodesy.sk/zbgiscsw/service.svc/get?REQUEST=GetRecordById&amp;SERVICE=CSW&amp;VERSION=2.0.2&amp;OUTPUTSCHEMA=http://www.isotc211.org/2005/gmd&amp;ELEMENTSETNAME=full&amp;Id=https://data.gov.sk/set/rpi/gmd/17316219/SK_UGKK_..."
19.12.2017 22:57:54 - "Test Assertion 'md-iso.d.3: Coupled resource': FAILED - 36278 ms"
19.12.2017 22:57:54 - "Test Case 'Service tests' finished: FAILED"
19.12.2017 22:57:54 - "Test Case 'Keywords' started"
19.12.2017 22:57:54 - "Test Assertion 'md-iso.e.1: Keywords': PASSED - 0 ms"
19.12.2017 22:57:54 - "Test Case 'Keywords' finished: PASSED"
19.12.2017 22:57:54 - "Test Case 'Keywords - details' started"
19.12.2017 22:57:54 - "Test Assertion 'md-iso.f.1: Dataset keyword': PASSED - 0 ms"
19.12.2017 22:57:54 - "Checking URL: 'http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/SpatialDataServiceCategory.en.atom'"
19.12.2017 22:57:54 - "Test Assertion 'md-iso.f.2: Service keyword': PASSED - 66 ms"
19.12.2017 22:57:54 - "Test Assertion 'md-iso.f.3: Keywords in vocabulary grouped': PASSED - 0 ms"
19.12.2017 22:57:54 - "Test Assertion 'md-iso.f.4: Vocabulary information': PASSED - 0 ms"
19.12.2017 22:57:54 - "Test Case 'Keywords - details' finished: PASSED"
19.12.2017 22:57:54 - "Test Case 'Temporal extent' started"
19.12.2017 22:57:54 - "Test Assertion 'md-iso.g.1: Temporal extent': PASSED - 0 ms"
19.12.2017 22:57:54 - "Test Case 'Temporal extent' finished: PASSED"
19.12.2017 22:57:54 - "Test Case 'Temporal extent - details' started"
19.12.2017 22:57:54 - "Test Assertion 'md-iso.h.1: Temporal date': PASSED - 0 ms"
19.12.2017 22:57:54 - "Test Case 'Temporal extent - details' finished: PASSED"
19.12.2017 22:57:54 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' finished: FAILED"
19.12.2017 22:57:56 - Releasing resources
19.12.2017 22:57:56 - Changed state from INITIALIZED to RUNNING
19.12.2017 22:57:56 - Duration: 41sec
19.12.2017 22:57:56 - TestRun finished
19.12.2017 22:57:56 - Changed state from RUNNING to COMPLETED
