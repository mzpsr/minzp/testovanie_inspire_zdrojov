31.01.2018 22:54:51 - Preparing Test Run interoperability_17316219_Geodetick&yacute;akartografick&yacute;&uacute;stavBratislava_hy-p:Lock (initiated Wed Jan 31 22:54:51 CET 2018)
31.01.2018 22:54:51 - Resolving Executable Test Suite dependencies
31.01.2018 22:54:51 - Preparing 10 Test Task:
31.01.2018 22:54:51 -  TestTask 1 (c8c4422f-a7e3-4660-8ff6-7def7ab25fc3)
31.01.2018 22:54:51 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML encoding (EID: 545f9e49-009b-4114-9333-7ca26413b5d4, V: 0.2.1 )'
31.01.2018 22:54:51 -  with parameters: 
31.01.2018 22:54:51 - etf.testcases = *
31.01.2018 22:54:51 -  TestTask 2 (2ef448ee-bf10-487c-8528-a53ca24eff6f)
31.01.2018 22:54:51 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.09820daf-62b2-4fa3-a95f-56a0d2b7c4d8'
31.01.2018 22:54:51 -  with parameters: 
31.01.2018 22:54:51 - etf.testcases = *
31.01.2018 22:54:51 -  TestTask 3 (0f0f9373-1126-482e-b893-1e861fb6e5ca)
31.01.2018 22:54:51 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.81b070d3-b17f-430b-abee-456268346912'
31.01.2018 22:54:51 -  with parameters: 
31.01.2018 22:54:51 - etf.testcases = *
31.01.2018 22:54:51 -  TestTask 4 (6d520d68-8a80-4d44-8fad-484b5aad753e)
31.01.2018 22:54:51 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Application schema, Hydrography - Physical Waters (EID: 45133c90-1929-405c-867d-9648b0620bf7, V: 0.2.1 )'
31.01.2018 22:54:51 -  with parameters: 
31.01.2018 22:54:51 - etf.testcases = *
31.01.2018 22:54:51 -  TestTask 5 (6177b44e-3f21-4b71-92b1-475d9ad9e59b)
31.01.2018 22:54:51 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.61070ae8-13cb-4303-a340-72c8b877b00a'
31.01.2018 22:54:51 -  with parameters: 
31.01.2018 22:54:51 - etf.testcases = *
31.01.2018 22:54:51 -  TestTask 6 (0069a84b-c1c5-4a8b-9474-b70ac0934d80)
31.01.2018 22:54:51 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Data consistency, Hydrography (EID: d0b58f38-98ae-43a8-a787-9a5084c60267, V: 0.2.2 )'
31.01.2018 22:54:51 -  with parameters: 
31.01.2018 22:54:51 - etf.testcases = *
31.01.2018 22:54:51 -  TestTask 7 (6adfdd7e-2cfc-4754-b12b-106701b95f0e)
31.01.2018 22:54:51 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.499937ea-0590-42d2-bd7a-1cafff35ecdb'
31.01.2018 22:54:51 -  with parameters: 
31.01.2018 22:54:51 - etf.testcases = *
31.01.2018 22:54:51 -  TestTask 8 (11a18377-1ee4-4545-ab63-bc330fd1a4af)
31.01.2018 22:54:51 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Information accessibility, Hydrography (EID: 893b7541-c9cb-4e0a-9f84-5d55cad1866c, V: 0.2.1 )'
31.01.2018 22:54:51 -  with parameters: 
31.01.2018 22:54:51 - etf.testcases = *
31.01.2018 22:54:51 -  TestTask 9 (69d67ee9-609f-43dd-930e-ae3502c7f383)
31.01.2018 22:54:51 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.63f586f0-080c-493b-8ca2-9919427440cc'
31.01.2018 22:54:51 -  with parameters: 
31.01.2018 22:54:51 - etf.testcases = *
31.01.2018 22:54:51 -  TestTask 10 (51f69407-f171-48d4-822a-2c3f0b987bc8)
31.01.2018 22:54:51 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Reference systems, Hydrography (EID: 122b2f38-302f-4271-9653-69cf86fcb5c4, V: 0.2.1 )'
31.01.2018 22:54:51 -  with parameters: 
31.01.2018 22:54:51 - etf.testcases = *
31.01.2018 22:54:51 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
31.01.2018 22:54:51 - Setting state to CREATED
31.01.2018 22:54:51 - Changed state from CREATED to INITIALIZING
31.01.2018 22:55:35 - Starting TestRun.2c9a620e-957a-4c2a-92c4-6fd19400556a at 2018-01-31T22:55:37+01:00
31.01.2018 22:55:37 - Changed state from INITIALIZING to INITIALIZED
31.01.2018 22:55:37 - TestRunTask initialized
31.01.2018 22:55:37 - Creating new tests databases to speed up tests.
31.01.2018 22:55:37 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:55:37 - Optimizing last database etf-tdb-7ad7c073-bbde-4fc2-9ead-7f19108b1310-0 
31.01.2018 22:55:37 - Import completed
31.01.2018 22:55:37 - Validation ended with 0 error(s)
31.01.2018 22:55:37 - Compiling test script
31.01.2018 22:55:37 - Starting XQuery tests
31.01.2018 22:55:37 - "Testing 1 features"
31.01.2018 22:55:37 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-encoding/inspire-gml/ets-inspire-gml-bsxets.xml"
31.01.2018 22:55:37 - "Statistics table: 1 ms"
31.01.2018 22:55:37 - "Test Suite 'Conformance class: INSPIRE GML encoding' started"
31.01.2018 22:55:37 - "Test Case 'Basic tests' started"
31.01.2018 22:55:37 - "Test Assertion 'gml.a.1: Errors loading the XML documents': PASSED - 0 ms"
31.01.2018 22:55:37 - "Test Assertion 'gml.a.2: Document root element': PASSED - 0 ms"
31.01.2018 22:55:37 - "Test Assertion 'gml.a.3: Character encoding': NOT_APPLICABLE"
31.01.2018 22:55:37 - "Test Case 'Basic tests' finished: PASSED"
31.01.2018 22:55:37 - "Test Suite 'Conformance class: INSPIRE GML encoding' finished: PASSED"
31.01.2018 22:55:37 - Releasing resources
31.01.2018 22:55:37 - TestRunTask initialized
31.01.2018 22:55:37 - Recreating new tests databases as the Test Object has changed!
31.01.2018 22:55:37 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:55:37 - Optimizing last database etf-tdb-7ad7c073-bbde-4fc2-9ead-7f19108b1310-0 
31.01.2018 22:55:37 - Import completed
31.01.2018 22:55:37 - Validation ended with 0 error(s)
31.01.2018 22:55:37 - Compiling test script
31.01.2018 22:55:37 - Starting XQuery tests
31.01.2018 22:55:38 - "Testing 1 features"
31.01.2018 22:55:38 - "Indexing features (parsing errors: 0): 35 ms"
31.01.2018 22:55:38 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/schemas/ets-schemas-bsxets.xml"
31.01.2018 22:55:38 - "Statistics table: 1 ms"
31.01.2018 22:55:38 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' started"
31.01.2018 22:55:38 - "Test Case 'Schema' started"
31.01.2018 22:55:38 - "Test Assertion 'gmlas.a.1: Mapping of source data to INSPIRE': PASSED_MANUAL"
31.01.2018 22:55:38 - "Test Assertion 'gmlas.a.2: Modelling of additional spatial object types': PASSED_MANUAL"
31.01.2018 22:55:38 - "Test Case 'Schema' finished: PASSED_MANUAL"
31.01.2018 22:55:38 - "Test Case 'Schema validation' started"
31.01.2018 22:55:38 - "Test Assertion 'gmlas.b.1: xsi:schemaLocation attribute': PASSED - 0 ms"
31.01.2018 22:55:38 - "Validating get.xml"
31.01.2018 22:55:47 - "Duration: 9587 ms. Errors: 1."
31.01.2018 22:55:47 - "Test Assertion 'gmlas.b.2: validate XML documents': FAILED - 9588 ms"
31.01.2018 22:55:47 - "Test Case 'Schema validation' finished: FAILED"
31.01.2018 22:55:47 - "Test Case 'GML model' started"
31.01.2018 22:55:47 - "Test Assertion 'gmlas.c.1: Consistency with the GML model': PASSED - 0 ms"
31.01.2018 22:55:47 - "Test Assertion 'gmlas.c.2: nilReason attributes require xsi:nil=true': PASSED - 0 ms"
31.01.2018 22:55:47 - "Test Assertion 'gmlas.c.3: nilReason values': PASSED - 0 ms"
31.01.2018 22:55:47 - "Test Case 'GML model' finished: PASSED"
31.01.2018 22:55:47 - "Test Case 'Simple features' started"
31.01.2018 22:55:47 - "Test Assertion 'gmlas.d.1: No spatial topology objects': PASSED - 0 ms"
31.01.2018 22:55:47 - "Test Assertion 'gmlas.d.2: No non-linear interpolation': PASSED - 0 ms"
31.01.2018 22:55:47 - "Test Assertion 'gmlas.d.3: Surface geometry elements': PASSED - 0 ms"
31.01.2018 22:55:47 - "Test Assertion 'gmlas.d.4: No non-planar interpolation': PASSED - 0 ms"
31.01.2018 22:55:47 - "Test Assertion 'gmlas.d.5: Geometry elements': PASSED - 0 ms"
31.01.2018 22:55:47 - "Test Assertion 'gmlas.d.6: Point coordinates in gml:pos': PASSED - 0 ms"
31.01.2018 22:55:47 - "Test Assertion 'gmlas.d.7: Curve/Surface coordinates in gml:posList': PASSED - 0 ms"
31.01.2018 22:55:47 - "Test Assertion 'gmlas.d.8: No array property elements': PASSED - 0 ms"
31.01.2018 22:55:47 - "Test Assertion 'gmlas.d.9: 1, 2 or 3 coordinate dimensions': PASSED - 0 ms"
31.01.2018 22:55:47 - "Test Assertion 'gmlas.d.10: Validate geometries (1)': PASSED - 37 ms"
31.01.2018 22:55:47 - "Test Assertion 'gmlas.d.11: Validate geometries (2)': PASSED - 0 ms"
31.01.2018 22:55:47 - "Test Case 'Simple features' finished: PASSED"
31.01.2018 22:55:47 - "Test Case 'Code list values in basic data types' started"
31.01.2018 22:55:47 - "Test Assertion 'gmlas.e.1: GrammaticalNumber attributes': PASSED - 6 ms"
31.01.2018 22:55:47 - "Test Assertion 'gmlas.e.2: GrammaticalGender attributes': PASSED - 3 ms"
31.01.2018 22:55:47 - "Test Assertion 'gmlas.e.3: NameStatus attributes': PASSED - 4 ms"
31.01.2018 22:55:47 - "Test Assertion 'gmlas.e.4: Nativeness attributes': PASSED - 2 ms"
31.01.2018 22:55:47 - "Test Case 'Code list values in basic data types' finished: PASSED"
31.01.2018 22:55:47 - "Test Case 'Constraints' started"
31.01.2018 22:55:47 - "Test Assertion 'gmlas.f.1: At least one of the two attributes pronunciationSoundLink and pronunciationIPA shall not be void': PASSED - 0 ms"
31.01.2018 22:55:47 - "Test Case 'Constraints' finished: PASSED"
31.01.2018 22:55:47 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' finished: FAILED"
31.01.2018 22:55:48 - Releasing resources
31.01.2018 22:55:48 - TestRunTask initialized
31.01.2018 22:55:48 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:55:48 - Validation ended with 0 error(s)
31.01.2018 22:55:48 - Compiling test script
31.01.2018 22:55:48 - Starting XQuery tests
31.01.2018 22:55:48 - "Testing 1 features"
31.01.2018 22:55:48 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-gml/ets-hy-gml-bsxets.xml"
31.01.2018 22:55:48 - "Statistics table: 0 ms"
31.01.2018 22:55:48 - "Test Suite 'Conformance class: GML application schemas, Hydrography' started"
31.01.2018 22:55:48 - "Test Case 'Basic test' started"
31.01.2018 22:55:48 - "Test Assertion 'hy-gml.a.1: Hydrographic feature in dataset': PASSED - 0 ms"
31.01.2018 22:55:48 - "Test Case 'Basic test' finished: PASSED"
31.01.2018 22:55:48 - "Test Suite 'Conformance class: GML application schemas, Hydrography' finished: PASSED"
31.01.2018 22:55:48 - Releasing resources
31.01.2018 22:55:48 - TestRunTask initialized
31.01.2018 22:55:48 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:55:48 - Validation ended with 0 error(s)
31.01.2018 22:55:48 - Compiling test script
31.01.2018 22:55:48 - Starting XQuery tests
31.01.2018 22:55:48 - "Testing 1 features"
31.01.2018 22:55:48 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-p-as/ets-hy-p-as-bsxets.xml"
31.01.2018 22:55:48 - "Statistics table: 0 ms"
31.01.2018 22:55:48 - "Test Suite 'Conformance class: Application schema, Hydrography - Physical Waters' started"
31.01.2018 22:55:48 - "Test Case 'Code list values' started"
31.01.2018 22:55:48 - "Test Assertion 'hy-p-as.a.1: condition attributes': PASSED - 5 ms"
31.01.2018 22:55:48 - "Test Assertion 'hy-p-as.a.2: type attributes': PASSED - 3 ms"
31.01.2018 22:55:48 - "Test Assertion 'hy-p-as.a.3: waterLevelCategory attributes': PASSED - 7 ms"
31.01.2018 22:55:48 - "Test Assertion 'hy-p-as.a.4: composition attributes': PASSED - 4 ms"
31.01.2018 22:55:48 - "Test Assertion 'hy-p-as.a.5: persistence attributes': PASSED - 3 ms"
31.01.2018 22:55:48 - "Test Case 'Code list values' finished: PASSED"
31.01.2018 22:55:48 - "Test Case 'Geometry' started"
31.01.2018 22:55:48 - "Test Assertion 'hy-p-as.b.1: Level of detail': PASSED_MANUAL"
31.01.2018 22:55:48 - "Test Case 'Geometry' finished: PASSED_MANUAL"
31.01.2018 22:55:48 - "Test Case 'Identifiers and references' started"
31.01.2018 22:55:48 - "Test Assertion 'hy-p-as.c.1: Reuse of authoritative, pan-European identifiers': PASSED_MANUAL"
31.01.2018 22:55:48 - "Test Case 'Identifiers and references' finished: PASSED_MANUAL"
31.01.2018 22:55:48 - "Test Case 'Constraints' started"
31.01.2018 22:55:48 - "Test Assertion 'hy-p-as.d.1: A river basin may not be contained in any other basin': PASSED - 0 ms"
31.01.2018 22:55:48 - "Test Assertion 'hy-p-as.d.2: A standing water geometry may be a surface or point': PASSED - 0 ms"
31.01.2018 22:55:48 - "Test Assertion 'hy-p-as.d.3: A watercourse geometry may be a curve or surface': PASSED - 0 ms"
31.01.2018 22:55:48 - "Test Assertion 'hy-p-as.d.4: A condition attribute may be specified only for a man-made watercourse': PASSED - 0 ms"
31.01.2018 22:55:48 - "Test Assertion 'hy-p-as.d.5: Shores on either side of a watercourse shall be provided as separate Shore objects': PASSED_MANUAL"
31.01.2018 22:55:48 - "Test Case 'Constraints' finished: PASSED_MANUAL"
31.01.2018 22:55:48 - "Test Suite 'Conformance class: Application schema, Hydrography - Physical Waters' finished: PASSED_MANUAL"
31.01.2018 22:55:49 - Releasing resources
31.01.2018 22:55:49 - TestRunTask initialized
31.01.2018 22:55:49 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:55:49 - Validation ended with 0 error(s)
31.01.2018 22:55:49 - Compiling test script
31.01.2018 22:55:49 - Starting XQuery tests
31.01.2018 22:55:49 - "Testing 1 features"
31.01.2018 22:55:49 - "Indexing features (parsing errors: 0): 40 ms"
31.01.2018 22:55:49 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/data-consistency/ets-data-consistency-bsxets.xml"
31.01.2018 22:55:49 - "Statistics table: 1 ms"
31.01.2018 22:55:49 - "Test Suite 'Conformance class: Data consistency, General requirements' started"
31.01.2018 22:55:49 - "Test Case 'Version consistency' started"
31.01.2018 22:55:49 - "Test Assertion 'dc.a.1: Version lifespan plausible': PASSED - 0 ms"
31.01.2018 22:55:49 - "Test Assertion 'dc.a.2: Unique identifier persistency': PASSED_MANUAL"
31.01.2018 22:55:49 - "Test Assertion 'dc.a.3: Spatial object type stable': PASSED_MANUAL"
31.01.2018 22:55:49 - "Test Case 'Version consistency' finished: PASSED_MANUAL"
31.01.2018 22:55:49 - "Test Case 'Temporal consistency' started"
31.01.2018 22:55:49 - "Test Assertion 'dc.b.1: Valid time plausible': PASSED - 0 ms"
31.01.2018 22:55:49 - "Test Case 'Temporal consistency' finished: PASSED"
31.01.2018 22:55:49 - "Test Suite 'Conformance class: Data consistency, General requirements' finished: PASSED_MANUAL"
31.01.2018 22:55:49 - Releasing resources
31.01.2018 22:55:49 - TestRunTask initialized
31.01.2018 22:55:49 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:55:49 - Validation ended with 0 error(s)
31.01.2018 22:55:49 - Compiling test script
31.01.2018 22:55:49 - Starting XQuery tests
31.01.2018 22:55:49 - "Testing 1 features"
31.01.2018 22:55:49 - "Indexing features (parsing errors: 0): 43 ms"
31.01.2018 22:55:49 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-dc/ets-hy-dc-bsxets.xml"
31.01.2018 22:55:49 - "Statistics table: 0 ms"
31.01.2018 22:55:49 - "Test Suite 'Conformance class: Data consistency, Hydrography' started"
31.01.2018 22:55:49 - "Test Case 'Spatial consistency' started"
31.01.2018 22:55:49 - "Test Assertion 'hy-dc.a.1: Each Network geometry is within a physical water geometry': PASSED - 0 ms"
31.01.2018 22:55:49 - "Test Assertion 'hy-dc.a.2: Manual review': PASSED_MANUAL"
31.01.2018 22:55:49 - "Test Case 'Spatial consistency' finished: PASSED_MANUAL"
31.01.2018 22:55:49 - "Test Case 'Thematic consistency' started"
31.01.2018 22:55:49 - "Test Assertion 'hy-dc.b.1: Consistency with Water Framework Directive reporting': PASSED_MANUAL"
31.01.2018 22:55:49 - "Test Case 'Thematic consistency' finished: PASSED_MANUAL"
31.01.2018 22:55:49 - "Test Case 'Identifiers' started"
31.01.2018 22:55:49 - "Test Assertion 'hy-dc.c.1: Reusing authoritative, pan-European sources': PASSED_MANUAL"
31.01.2018 22:55:49 - "Test Assertion 'hy-dc.c.2: Consistency with Water Framework Directive reporting': PASSED_MANUAL"
31.01.2018 22:55:49 - "Test Case 'Identifiers' finished: PASSED_MANUAL"
31.01.2018 22:55:49 - "Test Suite 'Conformance class: Data consistency, Hydrography' finished: PASSED_MANUAL"
31.01.2018 22:55:50 - Releasing resources
31.01.2018 22:55:50 - TestRunTask initialized
31.01.2018 22:55:50 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:55:50 - Validation ended with 0 error(s)
31.01.2018 22:55:50 - Compiling test script
31.01.2018 22:55:50 - Starting XQuery tests
31.01.2018 22:55:50 - "Testing 1 features"
31.01.2018 22:55:50 - "Indexing features (parsing errors: 0): 39 ms"
31.01.2018 22:55:50 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/information-accessibility/ets-information-accessibility-bsxets.xml"
31.01.2018 22:55:50 - "Statistics table: 1 ms"
31.01.2018 22:55:50 - "Test Suite 'Conformance class: Information accessibility, General requirements' started"
31.01.2018 22:55:50 - "Test Case 'Coordinate reference systems (CRS)' started"
31.01.2018 22:55:50 - "Test Assertion 'ia.a.1: CRS publicly accessible via HTTP': FAILED - 0 ms"
31.01.2018 22:55:50 - "Test Case 'Coordinate reference systems (CRS)' finished: FAILED"
31.01.2018 22:55:50 - "Test Suite 'Conformance class: Information accessibility, General requirements' finished: FAILED"
31.01.2018 22:55:50 - Releasing resources
31.01.2018 22:55:50 - TestRunTask initialized
31.01.2018 22:55:50 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:55:50 - Validation ended with 0 error(s)
31.01.2018 22:55:50 - Compiling test script
31.01.2018 22:55:50 - Starting XQuery tests
31.01.2018 22:55:50 - "Testing 1 features"
31.01.2018 22:55:50 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-ia/ets-hy-ia-bsxets.xml"
31.01.2018 22:55:50 - "Statistics table: 0 ms"
31.01.2018 22:55:50 - "Test Suite 'Conformance class: Information accessibility, Hydrography' started"
31.01.2018 22:55:50 - "Test Case 'Code lists' started"
31.01.2018 22:55:50 - "Test Assertion 'hy-ia.a.1: Code list extensions accessible': PASSED - 0 ms"
31.01.2018 22:55:50 - "Test Case 'Code lists' finished: PASSED"
31.01.2018 22:55:50 - "Test Case 'Feature references' started"
31.01.2018 22:55:50 - "Test Assertion 'hy-ia.b.1: HydroObject.relatedHydroObject': PASSED - 0 ms"
31.01.2018 22:55:50 - "Test Assertion 'hy-ia.b.2: WatercourseSeparatedCrossing.element': PASSED - 0 ms"
31.01.2018 22:55:50 - "Test Assertion 'hy-ia.b.3: WatercourseLink.startNode': PASSED - 1 ms"
31.01.2018 22:55:50 - "Test Assertion 'hy-ia.b.4: WatercourseLink.endNode': PASSED - 0 ms"
31.01.2018 22:55:50 - "Test Assertion 'hy-ia.b.5: SurfaceWater.bank': PASSED - 0 ms"
31.01.2018 22:55:50 - "Test Assertion 'hy-ia.b.6: SurfaceWater.drainsBasin': PASSED - 0 ms"
31.01.2018 22:55:50 - "Test Assertion 'hy-ia.b.7: SurfaceWater.neighbour': PASSED - 0 ms"
31.01.2018 22:55:50 - "Test Assertion 'hy-ia.b.8: DrainageBasin.outlet': PASSED - 0 ms"
31.01.2018 22:55:50 - "Test Case 'Feature references' finished: PASSED"
31.01.2018 22:55:50 - "Test Suite 'Conformance class: Information accessibility, Hydrography' finished: PASSED"
31.01.2018 22:55:51 - Releasing resources
31.01.2018 22:55:51 - TestRunTask initialized
31.01.2018 22:55:51 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:55:51 - Validation ended with 0 error(s)
31.01.2018 22:55:51 - Compiling test script
31.01.2018 22:55:51 - Starting XQuery tests
31.01.2018 22:55:51 - "Testing 1 features"
31.01.2018 22:55:51 - "Indexing features (parsing errors: 0): 41 ms"
31.01.2018 22:55:51 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/reference-systems/ets-reference-systems-bsxets.xml"
31.01.2018 22:55:51 - "Statistics table: 0 ms"
31.01.2018 22:55:51 - "Test Suite 'Conformance class: Reference systems, General requirements' started"
31.01.2018 22:55:51 - "Test Case 'Spatial reference systems' started"
31.01.2018 22:55:51 - "Test Assertion 'rs.a.1: Spatial reference systems in feature geometries': FAILED - 0 ms"
31.01.2018 22:55:51 - "Test Assertion 'rs.a.2: Default spatial reference systems in feature collections': PASSED - 0 ms"
31.01.2018 22:55:51 - "Test Case 'Spatial reference systems' finished: FAILED"
31.01.2018 22:55:51 - "Test Case 'Temporal reference systems' started"
31.01.2018 22:55:51 - "Test Assertion 'rs.a.3: Temporal reference systems in features': PASSED - 0 ms"
31.01.2018 22:55:51 - "Test Case 'Temporal reference systems' finished: PASSED"
31.01.2018 22:55:51 - "Test Suite 'Conformance class: Reference systems, General requirements' finished: FAILED"
31.01.2018 22:55:52 - Releasing resources
31.01.2018 22:55:52 - TestRunTask initialized
31.01.2018 22:55:52 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
31.01.2018 22:55:52 - Validation ended with 0 error(s)
31.01.2018 22:55:52 - Compiling test script
31.01.2018 22:55:52 - Starting XQuery tests
31.01.2018 22:55:52 - "Testing 1 features"
31.01.2018 22:55:52 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-rs/ets-hy-rs-bsxets.xml"
31.01.2018 22:55:52 - "Statistics table: 0 ms"
31.01.2018 22:55:52 - "Test Suite 'Conformance class: Reference systems, Hydrography' started"
31.01.2018 22:55:52 - "Test Case 'Units of measure' started"
31.01.2018 22:55:52 - "Test Assertion 'hy-rs.a.1: WatercourseLink.length': PASSED - 0 ms"
31.01.2018 22:55:52 - "Test Assertion 'hy-rs.a.2: DrainageBasin.area': PASSED - 0 ms"
31.01.2018 22:55:52 - "Test Assertion 'hy-rs.a.3: Falls.length': PASSED - 0 ms"
31.01.2018 22:55:52 - "Test Assertion 'hy-rs.a.4: StandingWater.elevation': PASSED - 0 ms"
31.01.2018 22:55:52 - "Test Assertion 'hy-rs.a.5: StandingWater.meanDepth': PASSED - 0 ms"
31.01.2018 22:55:52 - "Test Assertion 'hy-rs.a.6: StandingWater.surfaceArea': PASSED - 0 ms"
31.01.2018 22:55:52 - "Test Assertion 'hy-rs.a.7: Watercourse.length': PASSED - 0 ms"
31.01.2018 22:55:52 - "Test Assertion 'hy-rs.a.8: Watercourse.width.lower': PASSED - 0 ms"
31.01.2018 22:55:52 - "Test Assertion 'hy-rs.a.9: Watercourse.width.upper': PASSED - 0 ms"
31.01.2018 22:55:52 - "Test Case 'Units of measure' finished: PASSED"
31.01.2018 22:55:52 - "Test Suite 'Conformance class: Reference systems, Hydrography' finished: PASSED"
31.01.2018 22:55:52 - Releasing resources
31.01.2018 22:55:52 - Changed state from INITIALIZED to RUNNING
31.01.2018 22:55:52 - Duration: 1min
31.01.2018 22:55:52 - TestRun finished
31.01.2018 22:55:52 - Changed state from RUNNING to COMPLETED
