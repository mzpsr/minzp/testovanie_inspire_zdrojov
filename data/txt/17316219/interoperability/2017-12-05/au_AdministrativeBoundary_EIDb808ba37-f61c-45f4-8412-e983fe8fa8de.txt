05.12.2017 12:51:47 - Preparing Test Run interoperability_17316219_Geodetick&yacute; a kartografick&yacute; &uacute;stav Bratislava(GK&Uacute;)_au:AdministrativeBoundary (initiated Tue Dec 05 12:51:47 CET 2017)
05.12.2017 12:51:47 - Resolving Executable Test Suite dependencies
05.12.2017 12:51:47 - Preparing 10 Test Task:
05.12.2017 12:51:47 -  TestTask 1 (d0cdd391-6e9c-4f54-9a5a-9038f32323b9)
05.12.2017 12:51:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML encoding (EID: 545f9e49-009b-4114-9333-7ca26413b5d4, V: 0.2.1 )'
05.12.2017 12:51:47 -  with parameters: 
05.12.2017 12:51:47 - etf.testcases = *
05.12.2017 12:51:47 -  TestTask 2 (c1286d0f-9159-4695-a03a-48e025f96f4c)
05.12.2017 12:51:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements (EID: 09820daf-62b2-4fa3-a95f-56a0d2b7c4d8, V: 0.2.3 )'
05.12.2017 12:51:47 -  with parameters: 
05.12.2017 12:51:47 - etf.testcases = *
05.12.2017 12:51:47 -  TestTask 3 (e2940cb4-9bca-444a-8a1e-86dadd3fb5f8)
05.12.2017 12:51:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.47c569bc-677d-4ce3-8411-e2b29189332a'
05.12.2017 12:51:47 -  with parameters: 
05.12.2017 12:51:47 - etf.testcases = *
05.12.2017 12:51:47 -  TestTask 4 (354983cf-5e04-44fc-9449-3dcd4c138dfc)
05.12.2017 12:51:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Application schema, Administrative Units (EID: ddecef4b-89a3-4f9d-9246-a50b588fa5a2, V: 0.2.1 )'
05.12.2017 12:51:47 -  with parameters: 
05.12.2017 12:51:47 - etf.testcases = *
05.12.2017 12:51:47 -  TestTask 5 (b86a77f5-fcc3-4158-bf97-e071fa8de958)
05.12.2017 12:51:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.61070ae8-13cb-4303-a340-72c8b877b00a'
05.12.2017 12:51:47 -  with parameters: 
05.12.2017 12:51:47 - etf.testcases = *
05.12.2017 12:51:47 -  TestTask 6 (c7b5ae58-2812-4f60-9c1a-64852faf2768)
05.12.2017 12:51:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Data consistency, Administrative Units (EID: acc5931c-4ff0-499f-b916-3cda1603456b, V: 0.2.2 )'
05.12.2017 12:51:47 -  with parameters: 
05.12.2017 12:51:47 - etf.testcases = *
05.12.2017 12:51:47 -  TestTask 7 (bfce638d-35b6-45e0-9f2c-655c7623bb0f)
05.12.2017 12:51:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.499937ea-0590-42d2-bd7a-1cafff35ecdb'
05.12.2017 12:51:47 -  with parameters: 
05.12.2017 12:51:47 - etf.testcases = *
05.12.2017 12:51:47 -  TestTask 8 (6a774521-4540-435c-bf53-e8dafac10f34)
05.12.2017 12:51:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Information accessibility, Administrative Units (EID: ee28b75e-5c80-4370-838d-ab1b18e30b13, V: 0.2.1 )'
05.12.2017 12:51:47 -  with parameters: 
05.12.2017 12:51:47 - etf.testcases = *
05.12.2017 12:51:47 -  TestTask 9 (80c03c50-86fc-4323-bfaa-499eaaa2b3cf)
05.12.2017 12:51:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.63f586f0-080c-493b-8ca2-9919427440cc'
05.12.2017 12:51:47 -  with parameters: 
05.12.2017 12:51:47 - etf.testcases = *
05.12.2017 12:51:47 -  TestTask 10 (2a3522d8-b512-4c97-b5e5-6edb5cdf6520)
05.12.2017 12:51:47 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Reference systems, Administrative Units (EID: cafb75f8-5deb-4cca-89df-d3189322e97f, V: 0.2.0 )'
05.12.2017 12:51:47 -  with parameters: 
05.12.2017 12:51:47 - etf.testcases = *
05.12.2017 12:51:47 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
05.12.2017 12:51:47 - Setting state to CREATED
05.12.2017 12:51:47 - Changed state from CREATED to INITIALIZING
05.12.2017 12:51:47 - Starting TestRun.b808ba37-f61c-45f4-8412-e983fe8fa8de at 2017-12-05T12:51:48+01:00
05.12.2017 12:51:48 - Changed state from INITIALIZING to INITIALIZED
05.12.2017 12:51:48 - TestRunTask initialized
05.12.2017 12:51:48 - Creating new tests databases to speed up tests.
05.12.2017 12:51:48 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
05.12.2017 12:51:48 - Optimizing last database etf-tdb-5a42f867-52e4-4a9c-9945-1d0e32e73d22-0 
05.12.2017 12:51:48 - Import completed
05.12.2017 12:51:50 - Validation ended with 0 error(s)
05.12.2017 12:51:50 - Compiling test script
05.12.2017 12:51:50 - Starting XQuery tests
05.12.2017 12:51:50 - "Testing 1 features"
05.12.2017 12:51:50 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-encoding/inspire-gml/ets-inspire-gml-bsxets.xml"
05.12.2017 12:51:50 - "Statistics table: 0 ms"
05.12.2017 12:51:50 - "Test Suite 'Conformance class: INSPIRE GML encoding' started"
05.12.2017 12:51:50 - "Test Case 'Basic tests' started"
05.12.2017 12:51:50 - "Test Assertion 'gml.a.1: Errors loading the XML documents': PASSED - 0 ms"
05.12.2017 12:51:50 - "Test Assertion 'gml.a.2: Document root element': PASSED - 0 ms"
05.12.2017 12:51:50 - "Test Assertion 'gml.a.3: Character encoding': NOT_APPLICABLE"
05.12.2017 12:51:50 - "Test Case 'Basic tests' finished: PASSED"
05.12.2017 12:51:50 - "Test Suite 'Conformance class: INSPIRE GML encoding' finished: PASSED"
05.12.2017 12:51:53 - Releasing resources
05.12.2017 12:51:53 - TestRunTask initialized
05.12.2017 12:51:53 - Recreating new tests databases as the Test Object has changed!
05.12.2017 12:51:53 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
05.12.2017 12:51:53 - Optimizing last database etf-tdb-5a42f867-52e4-4a9c-9945-1d0e32e73d22-0 
05.12.2017 12:51:53 - Import completed
05.12.2017 12:51:53 - Validation ended with 0 error(s)
05.12.2017 12:51:53 - Compiling test script
05.12.2017 12:51:53 - Starting XQuery tests
05.12.2017 12:51:53 - "Testing 1 features"
05.12.2017 12:51:53 - "Indexing features (parsing errors: 0): 134 ms"
05.12.2017 12:51:53 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/schemas/ets-schemas-bsxets.xml"
05.12.2017 12:51:53 - "Statistics table: 0 ms"
05.12.2017 12:51:53 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' started"
05.12.2017 12:51:53 - "Test Case 'Schema' started"
05.12.2017 12:51:53 - "Test Assertion 'gmlas.a.1: Mapping of source data to INSPIRE': PASSED_MANUAL"
05.12.2017 12:51:53 - "Test Assertion 'gmlas.a.2: Modelling of additional spatial object types': PASSED_MANUAL"
05.12.2017 12:51:53 - "Test Case 'Schema' finished: PASSED_MANUAL"
05.12.2017 12:51:53 - "Test Case 'Schema validation' started"
05.12.2017 12:51:53 - "Test Assertion 'gmlas.b.1: xsi:schemaLocation attribute': PASSED - 0 ms"
05.12.2017 12:51:53 - "Validating get.xml"
05.12.2017 12:52:00 - "Duration: 6531 ms. Errors: 0."
05.12.2017 12:52:00 - "Test Assertion 'gmlas.b.2: validate XML documents': PASSED - 6532 ms"
05.12.2017 12:52:00 - "Test Case 'Schema validation' finished: PASSED"
05.12.2017 12:52:00 - "Test Case 'GML model' started"
05.12.2017 12:52:00 - "Test Assertion 'gmlas.c.1: Consistency with the GML model': PASSED - 0 ms"
05.12.2017 12:52:00 - "Test Assertion 'gmlas.c.2: nilReason attributes require xsi:nil=true': PASSED - 0 ms"
05.12.2017 12:52:00 - "Test Assertion 'gmlas.c.3: nilReason values': PASSED - 1 ms"
05.12.2017 12:52:00 - "Test Case 'GML model' finished: PASSED"
05.12.2017 12:52:00 - "Test Case 'Simple features' started"
05.12.2017 12:52:00 - "Test Assertion 'gmlas.d.1: No spatial topology objects': PASSED - 0 ms"
05.12.2017 12:52:00 - "Test Assertion 'gmlas.d.2: No non-linear interpolation': PASSED - 0 ms"
05.12.2017 12:52:00 - "Test Assertion 'gmlas.d.3: Surface geometry elements': PASSED - 0 ms"
05.12.2017 12:52:00 - "Test Assertion 'gmlas.d.4: No non-planar interpolation': PASSED - 0 ms"
05.12.2017 12:52:00 - "Test Assertion 'gmlas.d.5: Geometry elements': PASSED - 0 ms"
05.12.2017 12:52:00 - "Test Assertion 'gmlas.d.6: Point coordinates in gml:pos': PASSED - 0 ms"
05.12.2017 12:52:00 - "Test Assertion 'gmlas.d.7: Curve/Surface coordinates in gml:posList': PASSED - 1 ms"
05.12.2017 12:52:00 - "Test Assertion 'gmlas.d.8: No array property elements': PASSED - 0 ms"
05.12.2017 12:52:00 - "Test Assertion 'gmlas.d.9: 1, 2 or 3 coordinate dimensions': PASSED - 0 ms"
05.12.2017 12:52:00 - "Test Assertion 'gmlas.d.10: Validate geometries (1)': PASSED - 193 ms"
05.12.2017 12:52:00 - "Test Assertion 'gmlas.d.11: Validate geometries (2)': PASSED - 0 ms"
05.12.2017 12:52:00 - "Test Case 'Simple features' finished: PASSED"
05.12.2017 12:52:00 - "Test Case 'Code list values in basic data types' started"
05.12.2017 12:52:00 - "Test Assertion 'gmlas.e.1: GrammaticalNumber attributes': PASSED - 11 ms"
05.12.2017 12:52:00 - "Test Assertion 'gmlas.e.2: GrammaticalGender attributes': PASSED - 5 ms"
05.12.2017 12:52:00 - "Test Assertion 'gmlas.e.3: NameStatus attributes': PASSED - 4 ms"
05.12.2017 12:52:00 - "Test Assertion 'gmlas.e.4: Nativeness attributes': PASSED - 4 ms"
05.12.2017 12:52:00 - "Test Case 'Code list values in basic data types' finished: PASSED"
05.12.2017 12:52:00 - "Test Case 'Constraints' started"
05.12.2017 12:52:00 - "Test Assertion 'gmlas.f.1: At least one of the two attributes pronunciationSoundLink and pronunciationIPA shall not be void': PASSED - 0 ms"
05.12.2017 12:52:00 - "Test Case 'Constraints' finished: PASSED"
05.12.2017 12:52:00 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' finished: PASSED_MANUAL"
05.12.2017 12:52:01 - Releasing resources
05.12.2017 12:52:01 - TestRunTask initialized
05.12.2017 12:52:01 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
05.12.2017 12:52:01 - Validation ended with 0 error(s)
05.12.2017 12:52:01 - Compiling test script
05.12.2017 12:52:01 - Starting XQuery tests
05.12.2017 12:52:01 - "Testing 1 features"
05.12.2017 12:52:01 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-au/au-gml/ets-au-gml-bsxets.xml"
05.12.2017 12:52:01 - "Statistics table: 0 ms"
05.12.2017 12:52:01 - "Test Suite 'Conformance class: GML application schemas, Administrative Units' started"
05.12.2017 12:52:01 - "Test Case 'Basic test' started"
05.12.2017 12:52:01 - "Test Assertion 'au-gml.a.1: Administrative Unit feature in dataset': PASSED - 0 ms"
05.12.2017 12:52:01 - "Test Case 'Basic test' finished: PASSED"
05.12.2017 12:52:01 - "Test Suite 'Conformance class: GML application schemas, Administrative Units' finished: PASSED"
05.12.2017 12:52:02 - Releasing resources
05.12.2017 12:52:02 - TestRunTask initialized
05.12.2017 12:52:02 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
05.12.2017 12:52:02 - Validation ended with 0 error(s)
05.12.2017 12:52:02 - Compiling test script
05.12.2017 12:52:02 - Starting XQuery tests
05.12.2017 12:52:02 - "Testing 1 features"
05.12.2017 12:52:02 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-au/au-as/ets-au-as-bsxets.xml"
05.12.2017 12:52:02 - "Statistics table: 0 ms"
05.12.2017 12:52:02 - "Test Suite 'Conformance class: Application schema, Administrative Units' started"
05.12.2017 12:52:02 - "Test Case 'Code list values' started"
05.12.2017 12:52:02 - "Test Assertion 'au-as.a.1: nationalLevel attributes': PASSED - 4 ms"
05.12.2017 12:52:02 - "Test Case 'Code list values' finished: PASSED"
05.12.2017 12:52:02 - "Test Case 'Constraints' started"
05.12.2017 12:52:02 - "Test Assertion 'au-as.b.1: No unit at highest level can associate units at a higher level': PASSED - 0 ms"
05.12.2017 12:52:02 - "Test Assertion 'au-as.b.2: No unit at lowest level can associate units at lower level': PASSED - 0 ms"
05.12.2017 12:52:02 - "Test Assertion 'au-as.b.3: Association role condominium applies only for administrative units which nationalLevel=1st order': PASSED - 0 ms"
05.12.2017 12:52:02 - "Test Case 'Constraints' finished: PASSED"
05.12.2017 12:52:02 - "Test Suite 'Conformance class: Application schema, Administrative Units' finished: PASSED"
05.12.2017 12:52:03 - Releasing resources
05.12.2017 12:52:03 - TestRunTask initialized
05.12.2017 12:52:03 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
05.12.2017 12:52:03 - Validation ended with 0 error(s)
05.12.2017 12:52:03 - Compiling test script
05.12.2017 12:52:03 - Starting XQuery tests
05.12.2017 12:52:03 - "Testing 1 features"
05.12.2017 12:52:03 - "Indexing features (parsing errors: 0): 149 ms"
05.12.2017 12:52:03 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/data-consistency/ets-data-consistency-bsxets.xml"
05.12.2017 12:52:03 - "Statistics table: 0 ms"
05.12.2017 12:52:03 - "Test Suite 'Conformance class: Data consistency, General requirements' started"
05.12.2017 12:52:03 - "Test Case 'Version consistency' started"
05.12.2017 12:52:03 - "Test Assertion 'dc.a.1: Version lifespan plausible': PASSED - 1 ms"
05.12.2017 12:52:03 - "Test Assertion 'dc.a.2: Unique identifier persistency': PASSED_MANUAL"
05.12.2017 12:52:03 - "Test Assertion 'dc.a.3: Spatial object type stable': PASSED_MANUAL"
05.12.2017 12:52:03 - "Test Case 'Version consistency' finished: PASSED_MANUAL"
05.12.2017 12:52:03 - "Test Case 'Temporal consistency' started"
05.12.2017 12:52:03 - "Test Assertion 'dc.b.1: Valid time plausible': PASSED - 0 ms"
05.12.2017 12:52:03 - "Test Case 'Temporal consistency' finished: PASSED"
05.12.2017 12:52:03 - "Test Suite 'Conformance class: Data consistency, General requirements' finished: PASSED_MANUAL"
05.12.2017 12:52:03 - Releasing resources
05.12.2017 12:52:03 - TestRunTask initialized
05.12.2017 12:52:03 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
05.12.2017 12:52:03 - Validation ended with 0 error(s)
05.12.2017 12:52:03 - Compiling test script
05.12.2017 12:52:03 - Starting XQuery tests
05.12.2017 12:52:03 - "Testing 1 features"
05.12.2017 12:52:04 - "Indexing features (parsing errors: 0): 163 ms"
05.12.2017 12:52:04 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-au/au-dc/ets-au-dc-bsxets.xml"
05.12.2017 12:52:04 - "Statistics table: 0 ms"
05.12.2017 12:52:04 - "Test Suite 'Conformance class: Data consistency, Administrative Units' started"
05.12.2017 12:52:04 - "Test Case 'Higher Hierarchy Consistency' started"
05.12.2017 12:52:04 - "Test Assertion 'au-dc.a.1: Each administrative unit refers to exactly one administrative unit at a higher level except those corresponding to the country level unit representing a Member State or being a co-administered unit.': PASSED - 0 ms"
05.12.2017 12:52:04 - "Test Case 'Higher Hierarchy Consistency' finished: PASSED"
05.12.2017 12:52:04 - "Test Case 'Lower Hierarchy Consistency' started"
05.12.2017 12:52:04 - "Test Assertion 'au-dc.b.1: Each administrative unit refers to a lower level administrative unit, except those corresponding to the lowest level of the administrative hierarchy.': PASSED - 0 ms"
05.12.2017 12:52:04 - "Test Assertion 'au-dc.b.2: Each administrative unit refers to lower level administrative units on the next level, except those corresponding to the lowest level of the administrative hierarchy.': PASSED - 0 ms"
05.12.2017 12:52:04 - "Test Case 'Lower Hierarchy Consistency' finished: PASSED"
05.12.2017 12:52:04 - "Test Case 'Co-administered' started"
05.12.2017 12:52:04 - "Test Assertion 'au-dc.c.1: Each administrative unit co-administered by two or more administrative units uses the association role administeredBy, and the units co-administering this unit use the inverse role coAdminister.': PASSED - 0 ms"
05.12.2017 12:52:04 - "Test Case 'Co-administered' finished: PASSED"
05.12.2017 12:52:04 - "Test Case 'Condominium administration' started"
05.12.2017 12:52:04 - "Test Assertion 'au-dc.d.1: Each condominium is only administered by administrative units at country level.': PASSED - 0 ms"
05.12.2017 12:52:04 - "Test Case 'Condominium administration' finished: PASSED"
05.12.2017 12:52:04 - "Test Case 'Area' started"
05.12.2017 12:52:04 - "Test Assertion 'au-dc.e.1: Administrative units having the same level of administrative hierarchy do not conceptually share common areas.': PASSED - 0 ms"
05.12.2017 12:52:04 - "Test Case 'Area' finished: PASSED"
05.12.2017 12:52:04 - "Test Case 'Boundary' started"
05.12.2017 12:52:04 - "Test Assertion 'au-dc.f.1: The geometry of each instance administrative boundary corresponds to an edge in the topological structure formed by the complete boundary graph, including the boundaries of all levels.': FAILED - 96 ms"
05.12.2017 12:52:04 - "Test Case 'Boundary' finished: FAILED"
05.12.2017 12:52:04 - "Test Case 'Condominium spatial extent' started"
05.12.2017 12:52:04 - "Test Assertion 'au-dc.g.1: The spatial extent of a condominium is not part of the geometry representing the spatial extent of an administrative unit.': PASSED - 0 ms"
05.12.2017 12:52:04 - "Test Case 'Condominium spatial extent' finished: PASSED"
05.12.2017 12:52:04 - "Test Suite 'Conformance class: Data consistency, Administrative Units' finished: FAILED"
05.12.2017 12:52:04 - Releasing resources
05.12.2017 12:52:04 - TestRunTask initialized
05.12.2017 12:52:04 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
05.12.2017 12:52:04 - Validation ended with 0 error(s)
05.12.2017 12:52:04 - Compiling test script
05.12.2017 12:52:04 - Starting XQuery tests
05.12.2017 12:52:05 - "Testing 1 features"
05.12.2017 12:52:05 - "Indexing features (parsing errors: 0): 131 ms"
05.12.2017 12:52:05 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/information-accessibility/ets-information-accessibility-bsxets.xml"
05.12.2017 12:52:05 - "Statistics table: 0 ms"
05.12.2017 12:52:05 - "Test Suite 'Conformance class: Information accessibility, General requirements' started"
05.12.2017 12:52:05 - "Test Case 'Coordinate reference systems (CRS)' started"
05.12.2017 12:52:05 - "Test Assertion 'ia.a.1: CRS publicly accessible via HTTP': FAILED - 1 ms"
05.12.2017 12:52:05 - "Test Case 'Coordinate reference systems (CRS)' finished: FAILED"
05.12.2017 12:52:05 - "Test Suite 'Conformance class: Information accessibility, General requirements' finished: FAILED"
05.12.2017 12:52:05 - Releasing resources
05.12.2017 12:52:05 - TestRunTask initialized
05.12.2017 12:52:05 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
05.12.2017 12:52:05 - Validation ended with 0 error(s)
05.12.2017 12:52:05 - Compiling test script
05.12.2017 12:52:05 - Starting XQuery tests
05.12.2017 12:52:05 - "Testing 1 features"
05.12.2017 12:52:05 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-au/au-ia/ets-au-ia-bsxets.xml"
05.12.2017 12:52:05 - "Statistics table: 0 ms"
05.12.2017 12:52:05 - "Test Suite 'Conformance class: Information accessibility, Administrative Units' started"
05.12.2017 12:52:05 - "Test Case 'Code lists' started"
05.12.2017 12:52:05 - "Test Assertion 'au-ia.a.1: Code list extensions accessible': PASSED - 0 ms"
05.12.2017 12:52:05 - "Test Case 'Code lists' finished: PASSED"
05.12.2017 12:52:05 - "Test Case 'Feature references' started"
05.12.2017 12:52:05 - "Test Assertion 'au-ia.b.1: AdministrativeUnit.NUTS': PASSED - 0 ms"
05.12.2017 12:52:05 - "Test Assertion 'au-ia.b.2: AdministrativeUnit.Condominium': PASSED - 0 ms"
05.12.2017 12:52:05 - "Test Assertion 'au-ia.b.3: AdministrativeUnit.upperLevelUnit': PASSED - 0 ms"
05.12.2017 12:52:05 - "Test Assertion 'au-ia.b.4: AdministrativeUnit.lowerLevelUnit': PASSED - 0 ms"
05.12.2017 12:52:05 - "Test Assertion 'au-ia.b.5: AdministrativeUnit.administeredBy': PASSED - 0 ms"
05.12.2017 12:52:05 - "Test Assertion 'au-ia.b.6: AdministrativeUnit.coAdminister': PASSED - 0 ms"
05.12.2017 12:52:05 - "Test Assertion 'au-ia.b.7: AdministrativeUnit.boundary': PASSED - 0 ms"
05.12.2017 12:52:05 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=http://inspire.ec.europa.eu/codelist/AdministrativeHierarchyLevel/4thOrder'"
05.12.2017 12:52:36 - "Exception: Timeout exceeded. URL: https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?SERVICE=WFS&amp;VERSION=2.0.0&amp;REQUEST=GetFeature&amp;STOREDQUERY_ID=urn:ogc:def:query:OGC-WFS::GetFeatureById&amp;id=http://inspire.ec.europa.eu/codelist/AdministrativeHierarchyLevel/4thOrder"
05.12.2017 12:52:36 - "Test Assertion 'au-ia.b.8: AdministrativeBoundary.admUnit and Condominium.admUnit': FAILED - 30116 ms"
05.12.2017 12:52:36 - "Test Assertion 'au-ia.b.9: MaritimeZone.boundary': PASSED - 0 ms"
05.12.2017 12:52:36 - "Test Assertion 'au-ia.b.10: MaritimeZone.seaArea': PASSED - 0 ms"
05.12.2017 12:52:36 - "Test Assertion 'au-ia.b.11: MaritimeZone.baseline': PASSED - 0 ms"
05.12.2017 12:52:36 - "Test Case 'Feature references' finished: FAILED"
05.12.2017 12:52:36 - "Test Suite 'Conformance class: Information accessibility, Administrative Units' finished: FAILED"
05.12.2017 12:52:36 - Releasing resources
05.12.2017 12:52:36 - TestRunTask initialized
05.12.2017 12:52:36 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
05.12.2017 12:52:36 - Validation ended with 0 error(s)
05.12.2017 12:52:36 - Compiling test script
05.12.2017 12:52:36 - Starting XQuery tests
05.12.2017 12:52:36 - "Testing 1 features"
05.12.2017 12:52:37 - "Indexing features (parsing errors: 0): 166 ms"
05.12.2017 12:52:37 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/reference-systems/ets-reference-systems-bsxets.xml"
05.12.2017 12:52:37 - "Statistics table: 0 ms"
05.12.2017 12:52:37 - "Test Suite 'Conformance class: Reference systems, General requirements' started"
05.12.2017 12:52:37 - "Test Case 'Spatial reference systems' started"
05.12.2017 12:52:37 - "Test Assertion 'rs.a.1: Spatial reference systems in feature geometries': FAILED - 0 ms"
05.12.2017 12:52:37 - "Test Assertion 'rs.a.2: Default spatial reference systems in feature collections': PASSED - 0 ms"
05.12.2017 12:52:37 - "Test Case 'Spatial reference systems' finished: FAILED"
05.12.2017 12:52:37 - "Test Case 'Temporal reference systems' started"
05.12.2017 12:52:37 - "Test Assertion 'rs.a.3: Temporal reference systems in features': PASSED - 0 ms"
05.12.2017 12:52:37 - "Test Case 'Temporal reference systems' finished: PASSED"
05.12.2017 12:52:37 - "Test Suite 'Conformance class: Reference systems, General requirements' finished: FAILED"
05.12.2017 12:52:37 - Releasing resources
05.12.2017 12:52:37 - TestRunTask initialized
05.12.2017 12:52:37 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
05.12.2017 12:52:37 - Validation ended with 0 error(s)
05.12.2017 12:52:37 - Compiling test script
05.12.2017 12:52:37 - Starting XQuery tests
05.12.2017 12:52:37 - "Testing 1 features"
05.12.2017 12:52:37 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-au/au-rs/ets-au-rs-bsxets.xml"
05.12.2017 12:52:37 - "Statistics table: 0 ms"
05.12.2017 12:52:37 - "Test Suite 'Conformance class: Reference systems, Administrative Units' started"
05.12.2017 12:52:37 - "Test Case 'Additional theme-specific rules for reference systems' started"
05.12.2017 12:52:37 - "Test Assertion 'au-rs.a.1: Test always passes': PASSED - 0 ms"
05.12.2017 12:52:37 - "Test Case 'Additional theme-specific rules for reference systems' finished: PASSED"
05.12.2017 12:52:37 - "Test Suite 'Conformance class: Reference systems, Administrative Units' finished: PASSED"
05.12.2017 12:52:37 - Releasing resources
05.12.2017 12:52:37 - Changed state from INITIALIZED to RUNNING
05.12.2017 12:52:38 - Duration: 50sec
05.12.2017 12:52:38 - TestRun finished
05.12.2017 12:52:38 - Changed state from RUNNING to COMPLETED
