05.02.2018 21:59:46 - Preparing Test Run interoperability_17316219_Geodetick&yacute;akartografick&yacute;&uacute;stavBratislava(GK&Uacute;)_gn:NamedPlace (initiated Mon Feb 05 21:59:46 CET 2018)
05.02.2018 21:59:46 - Resolving Executable Test Suite dependencies
05.02.2018 21:59:46 - Preparing 10 Test Task:
05.02.2018 21:59:46 -  TestTask 1 (10c7575c-f91b-4d7a-845d-21ddcc0dda6c)
05.02.2018 21:59:46 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements (EID: 09820daf-62b2-4fa3-a95f-56a0d2b7c4d8, V: 0.2.4 )'
05.02.2018 21:59:46 -  with parameters: 
05.02.2018 21:59:46 - etf.testcases = *
05.02.2018 21:59:46 -  TestTask 2 (47417484-584d-4eae-b3a2-25cfa8b0fdfd)
05.02.2018 21:59:46 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.02b7b0cb-429a-4f4e-b0db-988464fb9496'
05.02.2018 21:59:46 -  with parameters: 
05.02.2018 21:59:46 - etf.testcases = *
05.02.2018 21:59:46 -  TestTask 3 (f0cc7b0e-6172-4aa5-8b60-00b1acd049f8)
05.02.2018 21:59:46 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Application schema, Geographical Names (EID: 0fc46305-c623-422b-b7d7-251c3b86eb7f, V: 0.2.1 )'
05.02.2018 21:59:46 -  with parameters: 
05.02.2018 21:59:46 - etf.testcases = *
05.02.2018 21:59:46 -  TestTask 4 (44c07469-b5f1-41fd-b087-4216a582b0ad)
05.02.2018 21:59:46 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML encoding (EID: 545f9e49-009b-4114-9333-7ca26413b5d4, V: 0.2.1 )'
05.02.2018 21:59:46 -  with parameters: 
05.02.2018 21:59:46 - etf.testcases = *
05.02.2018 21:59:46 -  TestTask 5 (da81363e-229b-430f-8ec4-6dc82155420a)
05.02.2018 21:59:46 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.61070ae8-13cb-4303-a340-72c8b877b00a'
05.02.2018 21:59:46 -  with parameters: 
05.02.2018 21:59:46 - etf.testcases = *
05.02.2018 21:59:46 -  TestTask 6 (0fa274d1-e352-42fa-916e-2700e1d0ddfc)
05.02.2018 21:59:46 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Data consistency, Geographical Names (EID: a32f76c7-f1d3-4d70-83ef-d51d2545fa2e, V: 0.2.0 )'
05.02.2018 21:59:46 -  with parameters: 
05.02.2018 21:59:46 - etf.testcases = *
05.02.2018 21:59:46 -  TestTask 7 (46290a06-108f-4a05-82f6-4003932fd1d0)
05.02.2018 21:59:46 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.499937ea-0590-42d2-bd7a-1cafff35ecdb'
05.02.2018 21:59:46 -  with parameters: 
05.02.2018 21:59:46 - etf.testcases = *
05.02.2018 21:59:46 -  TestTask 8 (86417eec-111d-4d15-a4bf-c21893297c55)
05.02.2018 21:59:46 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Information accessibility, Geographical Names (EID: c3379b85-853e-4a35-8c3d-b64191d94587, V: 0.2.1 )'
05.02.2018 21:59:46 -  with parameters: 
05.02.2018 21:59:46 - etf.testcases = *
05.02.2018 21:59:46 -  TestTask 9 (b542c253-3acb-4fff-8516-5e2ea3c9cea6)
05.02.2018 21:59:46 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.63f586f0-080c-493b-8ca2-9919427440cc'
05.02.2018 21:59:46 -  with parameters: 
05.02.2018 21:59:46 - etf.testcases = *
05.02.2018 21:59:46 -  TestTask 10 (10b1291e-351c-4651-8005-4372d5e90f11)
05.02.2018 21:59:46 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Reference systems, Geographical Names (EID: 1620bd27-b881-48a2-bf2b-301541e035f4, V: 0.2.0 )'
05.02.2018 21:59:46 -  with parameters: 
05.02.2018 21:59:46 - etf.testcases = *
05.02.2018 21:59:46 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
05.02.2018 21:59:46 - Setting state to CREATED
05.02.2018 21:59:46 - Changed state from CREATED to INITIALIZING
05.02.2018 21:59:46 - Starting TestRun.699a69d7-4c9a-4333-b964-c25419bcb9f0 at 2018-02-05T21:59:48+01:00
05.02.2018 21:59:48 - Changed state from INITIALIZING to INITIALIZED
05.02.2018 21:59:48 - TestRunTask initialized
05.02.2018 21:59:48 - Creating new tests databases to speed up tests.
05.02.2018 21:59:48 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
05.02.2018 21:59:48 - Optimizing last database etf-tdb-f8ffba6c-c92c-4861-a218-c2e66ec67337-0 
05.02.2018 21:59:48 - Import completed
05.02.2018 21:59:48 - Validation ended with 0 error(s)
05.02.2018 21:59:48 - Compiling test script
05.02.2018 21:59:48 - Starting XQuery tests
05.02.2018 21:59:48 - "Testing 1 features"
05.02.2018 21:59:49 - "Indexing features (parsing errors: 0): 95 ms"
05.02.2018 21:59:49 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/schemas/ets-schemas-bsxets.xml"
05.02.2018 21:59:49 - "Statistics table: 1 ms"
05.02.2018 21:59:49 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' started"
05.02.2018 21:59:49 - "Test Case 'Schema' started"
05.02.2018 21:59:49 - "Test Assertion 'gmlas.a.1: Mapping of source data to INSPIRE': PASSED_MANUAL"
05.02.2018 21:59:49 - "Test Assertion 'gmlas.a.2: Modelling of additional spatial object types': PASSED_MANUAL"
05.02.2018 21:59:49 - "Test Case 'Schema' finished: PASSED_MANUAL"
05.02.2018 21:59:49 - "Test Case 'Schema validation' started"
05.02.2018 21:59:49 - "Test Assertion 'gmlas.b.1: xsi:schemaLocation attribute': PASSED - 0 ms"
05.02.2018 21:59:49 - "Validating get.xml"
05.02.2018 21:59:56 - "Duration: 7542 ms. Errors: 0."
05.02.2018 21:59:56 - "Test Assertion 'gmlas.b.2: validate XML documents': PASSED - 7544 ms"
05.02.2018 21:59:56 - "Test Case 'Schema validation' finished: PASSED"
05.02.2018 21:59:56 - "Test Case 'GML model' started"
05.02.2018 21:59:56 - "Test Assertion 'gmlas.c.1: Consistency with the GML model': PASSED - 1 ms"
05.02.2018 21:59:56 - "Test Assertion 'gmlas.c.2: nilReason attributes require xsi:nil=true': PASSED - 0 ms"
05.02.2018 21:59:56 - "Test Assertion 'gmlas.c.3: nilReason values': PASSED - 0 ms"
05.02.2018 21:59:56 - "Test Case 'GML model' finished: PASSED"
05.02.2018 21:59:56 - "Test Case 'Simple features' started"
05.02.2018 21:59:56 - "Test Assertion 'gmlas.d.1: No spatial topology objects': PASSED - 0 ms"
05.02.2018 21:59:56 - "Test Assertion 'gmlas.d.2: No non-linear interpolation': PASSED - 0 ms"
05.02.2018 21:59:56 - "Test Assertion 'gmlas.d.3: Surface geometry elements': PASSED - 0 ms"
05.02.2018 21:59:56 - "Test Assertion 'gmlas.d.4: No non-planar interpolation': PASSED - 0 ms"
05.02.2018 21:59:56 - "Test Assertion 'gmlas.d.5: Geometry elements': PASSED - 0 ms"
05.02.2018 21:59:56 - "Test Assertion 'gmlas.d.6: Point coordinates in gml:pos': PASSED - 0 ms"
05.02.2018 21:59:56 - "Test Assertion 'gmlas.d.7: Curve/Surface coordinates in gml:posList': PASSED - 0 ms"
05.02.2018 21:59:56 - "Test Assertion 'gmlas.d.8: No array property elements': PASSED - 0 ms"
05.02.2018 21:59:56 - "Test Assertion 'gmlas.d.9: 1, 2 or 3 coordinate dimensions': PASSED - 0 ms"
05.02.2018 21:59:56 - "Test Assertion 'gmlas.d.10: Validate geometries (1)': PASSED - 30 ms"
05.02.2018 21:59:56 - "Test Assertion 'gmlas.d.11: Validate geometries (2)': PASSED - 0 ms"
05.02.2018 21:59:56 - "Test Case 'Simple features' finished: PASSED"
05.02.2018 21:59:56 - "Test Case 'Code list values in basic data types' started"
05.02.2018 21:59:56 - "Test Assertion 'gmlas.e.1: GrammaticalNumber attributes': PASSED - 43 ms"
05.02.2018 21:59:56 - "Test Assertion 'gmlas.e.2: GrammaticalGender attributes': PASSED - 4 ms"
05.02.2018 21:59:56 - "Test Assertion 'gmlas.e.3: NameStatus attributes': PASSED - 3 ms"
05.02.2018 21:59:56 - "Test Assertion 'gmlas.e.4: Nativeness attributes': PASSED - 4 ms"
05.02.2018 21:59:56 - "Test Case 'Code list values in basic data types' finished: PASSED"
05.02.2018 21:59:56 - "Test Case 'Constraints' started"
05.02.2018 21:59:56 - "Test Assertion 'gmlas.f.1: At least one of the two attributes pronunciationSoundLink and pronunciationIPA shall not be void': PASSED - 0 ms"
05.02.2018 21:59:56 - "Test Case 'Constraints' finished: PASSED"
05.02.2018 21:59:56 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' finished: PASSED_MANUAL"
05.02.2018 21:59:57 - Releasing resources
05.02.2018 21:59:57 - TestRunTask initialized
05.02.2018 21:59:57 - Recreating new tests databases as the Test Object has changed!
05.02.2018 21:59:57 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
05.02.2018 21:59:57 - Optimizing last database etf-tdb-f8ffba6c-c92c-4861-a218-c2e66ec67337-0 
05.02.2018 21:59:57 - Import completed
05.02.2018 21:59:57 - Validation ended with 0 error(s)
05.02.2018 21:59:57 - Compiling test script
05.02.2018 21:59:57 - Starting XQuery tests
05.02.2018 21:59:57 - "Testing 1 features"
05.02.2018 21:59:57 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-gn/gn-gml/ets-gn-gml-bsxets.xml"
05.02.2018 21:59:57 - "Statistics table: 1 ms"
05.02.2018 21:59:57 - "Test Suite 'Conformance class: GML application schemas, Geographical Names' started"
05.02.2018 21:59:57 - "Test Case 'Basic test' started"
05.02.2018 21:59:57 - "Test Assertion 'gn-gml.a.1: Geographical Names feature in dataset': PASSED - 0 ms"
05.02.2018 21:59:57 - "Test Case 'Basic test' finished: PASSED"
05.02.2018 21:59:57 - "Test Suite 'Conformance class: GML application schemas, Geographical Names' finished: PASSED"
05.02.2018 21:59:57 - Releasing resources
05.02.2018 21:59:57 - TestRunTask initialized
05.02.2018 21:59:57 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
05.02.2018 21:59:57 - Validation ended with 0 error(s)
05.02.2018 21:59:57 - Compiling test script
05.02.2018 21:59:57 - Starting XQuery tests
05.02.2018 21:59:57 - "Testing 1 features"
05.02.2018 21:59:57 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-gn/gn-as/ets-gn-as-bsxets.xml"
05.02.2018 21:59:57 - "Statistics table: 0 ms"
05.02.2018 21:59:57 - "Test Suite 'Conformance class: Application schema, Geographical Names' started"
05.02.2018 21:59:57 - "Test Case 'Code list values' started"
05.02.2018 21:59:57 - "Test Assertion 'gn-as.a.1: NamedPlaceType attributes': PASSED - 23 ms"
05.02.2018 21:59:57 - "Test Case 'Code list values' finished: PASSED"
05.02.2018 21:59:57 - "Test Case 'Constraints' started"
05.02.2018 21:59:57 - "Test Assertion 'gn-as.b.1: Test always passes': PASSED - 0 ms"
05.02.2018 21:59:57 - "Test Case 'Constraints' finished: PASSED"
05.02.2018 21:59:57 - "Test Suite 'Conformance class: Application schema, Geographical Names' finished: PASSED"
05.02.2018 21:59:57 - Releasing resources
05.02.2018 21:59:57 - TestRunTask initialized
05.02.2018 21:59:57 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
05.02.2018 21:59:57 - Validation ended with 0 error(s)
05.02.2018 21:59:57 - Compiling test script
05.02.2018 21:59:57 - Starting XQuery tests
05.02.2018 21:59:57 - "Testing 1 features"
05.02.2018 21:59:57 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-encoding/inspire-gml/ets-inspire-gml-bsxets.xml"
05.02.2018 21:59:57 - "Statistics table: 1 ms"
05.02.2018 21:59:57 - "Test Suite 'Conformance class: INSPIRE GML encoding' started"
05.02.2018 21:59:57 - "Test Case 'Basic tests' started"
05.02.2018 21:59:57 - "Test Assertion 'gml.a.1: Errors loading the XML documents': PASSED - 0 ms"
05.02.2018 21:59:57 - "Test Assertion 'gml.a.2: Document root element': PASSED - 0 ms"
05.02.2018 21:59:57 - "Test Assertion 'gml.a.3: Character encoding': NOT_APPLICABLE"
05.02.2018 21:59:57 - "Test Case 'Basic tests' finished: PASSED"
05.02.2018 21:59:57 - "Test Suite 'Conformance class: INSPIRE GML encoding' finished: PASSED"
05.02.2018 21:59:58 - Releasing resources
05.02.2018 21:59:58 - TestRunTask initialized
05.02.2018 21:59:58 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
05.02.2018 21:59:58 - Validation ended with 0 error(s)
05.02.2018 21:59:58 - Compiling test script
05.02.2018 21:59:58 - Starting XQuery tests
05.02.2018 21:59:58 - "Testing 1 features"
05.02.2018 21:59:58 - "Indexing features (parsing errors: 0): 46 ms"
05.02.2018 21:59:58 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/data-consistency/ets-data-consistency-bsxets.xml"
05.02.2018 21:59:58 - "Statistics table: 0 ms"
05.02.2018 21:59:58 - "Test Suite 'Conformance class: Data consistency, General requirements' started"
05.02.2018 21:59:58 - "Test Case 'Version consistency' started"
05.02.2018 21:59:58 - "Test Assertion 'dc.a.1: Version lifespan plausible': PASSED - 0 ms"
05.02.2018 21:59:58 - "Test Assertion 'dc.a.2: Unique identifier persistency': PASSED_MANUAL"
05.02.2018 21:59:58 - "Test Assertion 'dc.a.3: Spatial object type stable': PASSED_MANUAL"
05.02.2018 21:59:58 - "Test Case 'Version consistency' finished: PASSED_MANUAL"
05.02.2018 21:59:58 - "Test Case 'Temporal consistency' started"
05.02.2018 21:59:58 - "Test Assertion 'dc.b.1: Valid time plausible': PASSED - 0 ms"
05.02.2018 21:59:58 - "Test Case 'Temporal consistency' finished: PASSED"
05.02.2018 21:59:58 - "Test Suite 'Conformance class: Data consistency, General requirements' finished: PASSED_MANUAL"
05.02.2018 21:59:58 - Releasing resources
05.02.2018 21:59:58 - TestRunTask initialized
05.02.2018 21:59:58 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
05.02.2018 21:59:58 - Validation ended with 0 error(s)
05.02.2018 21:59:58 - Compiling test script
05.02.2018 21:59:58 - Starting XQuery tests
05.02.2018 21:59:58 - "Testing 1 features"
05.02.2018 21:59:58 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-gn/gn-dc/ets-gn-dc-bsxets.xml"
05.02.2018 21:59:58 - "Statistics table: 0 ms"
05.02.2018 21:59:58 - "Test Suite 'Conformance class: Data consistency, Geographical Names' started"
05.02.2018 21:59:58 - "Test Case 'Additional theme-specific consistency rules' started"
05.02.2018 21:59:58 - "Test Assertion 'gn-dc.a.1: Test always passes': PASSED - 0 ms"
05.02.2018 21:59:58 - "Test Case 'Additional theme-specific consistency rules' finished: PASSED"
05.02.2018 21:59:58 - "Test Suite 'Conformance class: Data consistency, Geographical Names' finished: PASSED"
05.02.2018 21:59:58 - Releasing resources
05.02.2018 21:59:58 - TestRunTask initialized
05.02.2018 21:59:58 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
05.02.2018 21:59:58 - Validation ended with 0 error(s)
05.02.2018 21:59:58 - Compiling test script
05.02.2018 21:59:58 - Starting XQuery tests
05.02.2018 21:59:58 - "Testing 1 features"
05.02.2018 21:59:58 - "Indexing features (parsing errors: 0): 58 ms"
05.02.2018 21:59:58 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/information-accessibility/ets-information-accessibility-bsxets.xml"
05.02.2018 21:59:58 - "Statistics table: 0 ms"
05.02.2018 21:59:58 - "Test Suite 'Conformance class: Information accessibility, General requirements' started"
05.02.2018 21:59:58 - "Test Case 'Coordinate reference systems (CRS)' started"
05.02.2018 21:59:58 - "Test Assertion 'ia.a.1: CRS publicly accessible via HTTP': FAILED - 1 ms"
05.02.2018 21:59:58 - "Test Case 'Coordinate reference systems (CRS)' finished: FAILED"
05.02.2018 21:59:58 - "Test Suite 'Conformance class: Information accessibility, General requirements' finished: FAILED"
05.02.2018 21:59:59 - Releasing resources
05.02.2018 21:59:59 - TestRunTask initialized
05.02.2018 21:59:59 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
05.02.2018 21:59:59 - Validation ended with 0 error(s)
05.02.2018 21:59:59 - Compiling test script
05.02.2018 21:59:59 - Starting XQuery tests
05.02.2018 21:59:59 - "Testing 1 features"
05.02.2018 21:59:59 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-gn/gn-ia/ets-gn-ia-bsxets.xml"
05.02.2018 21:59:59 - "Statistics table: 0 ms"
05.02.2018 21:59:59 - "Test Suite 'Conformance class: Information accessibility, Geographical Names' started"
05.02.2018 21:59:59 - "Test Case 'Code lists' started"
05.02.2018 21:59:59 - "Test Assertion 'gn-ia.a.1: Code list extensions accessible': PASSED - 0 ms"
05.02.2018 21:59:59 - "Test Case 'Code lists' finished: PASSED"
05.02.2018 21:59:59 - "Test Case 'Feature references' started"
05.02.2018 21:59:59 - "Test Assertion 'gn-ia.b.1: referenced features retrievable': PASSED - 0 ms"
05.02.2018 21:59:59 - "Test Case 'Feature references' finished: PASSED"
05.02.2018 21:59:59 - "Test Suite 'Conformance class: Information accessibility, Geographical Names' finished: PASSED"
05.02.2018 21:59:59 - Releasing resources
05.02.2018 21:59:59 - TestRunTask initialized
05.02.2018 21:59:59 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
05.02.2018 21:59:59 - Validation ended with 0 error(s)
05.02.2018 21:59:59 - Compiling test script
05.02.2018 21:59:59 - Starting XQuery tests
05.02.2018 21:59:59 - "Testing 1 features"
05.02.2018 21:59:59 - "Indexing features (parsing errors: 0): 42 ms"
05.02.2018 21:59:59 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/reference-systems/ets-reference-systems-bsxets.xml"
05.02.2018 21:59:59 - "Statistics table: 0 ms"
05.02.2018 21:59:59 - "Test Suite 'Conformance class: Reference systems, General requirements' started"
05.02.2018 21:59:59 - "Test Case 'Spatial reference systems' started"
05.02.2018 21:59:59 - "Test Assertion 'rs.a.1: Spatial reference systems in feature geometries': FAILED - 1 ms"
05.02.2018 21:59:59 - "Test Assertion 'rs.a.2: Default spatial reference systems in feature collections': PASSED - 0 ms"
05.02.2018 21:59:59 - "Test Case 'Spatial reference systems' finished: FAILED"
05.02.2018 21:59:59 - "Test Case 'Temporal reference systems' started"
05.02.2018 21:59:59 - "Test Assertion 'rs.a.3: Temporal reference systems in features': PASSED - 0 ms"
05.02.2018 21:59:59 - "Test Case 'Temporal reference systems' finished: PASSED"
05.02.2018 21:59:59 - "Test Suite 'Conformance class: Reference systems, General requirements' finished: FAILED"
05.02.2018 22:00:00 - Releasing resources
05.02.2018 22:00:00 - TestRunTask initialized
05.02.2018 22:00:00 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
05.02.2018 22:00:00 - Validation ended with 0 error(s)
05.02.2018 22:00:00 - Compiling test script
05.02.2018 22:00:00 - Starting XQuery tests
05.02.2018 22:00:00 - "Testing 1 features"
05.02.2018 22:00:00 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-gn/gn-rs/ets-gn-rs-bsxets.xml"
05.02.2018 22:00:00 - "Statistics table: 1 ms"
05.02.2018 22:00:00 - "Test Suite 'Conformance class: Reference systems, Geographical Names' started"
05.02.2018 22:00:00 - "Test Case 'Additional theme-specific rules for reference systems' started"
05.02.2018 22:00:00 - "Test Assertion 'gn-rs.a.1: Test always passes': PASSED - 0 ms"
05.02.2018 22:00:00 - "Test Case 'Additional theme-specific rules for reference systems' finished: PASSED"
05.02.2018 22:00:00 - "Test Suite 'Conformance class: Reference systems, Geographical Names' finished: PASSED"
05.02.2018 22:00:00 - Releasing resources
05.02.2018 22:00:00 - Changed state from INITIALIZED to RUNNING
05.02.2018 22:00:00 - Duration: 14sec
05.02.2018 22:00:00 - TestRun finished
05.02.2018 22:00:00 - Changed state from RUNNING to COMPLETED
