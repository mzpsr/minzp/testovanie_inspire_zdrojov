08.11.2017 22:34:17 - Preparing Test Run metadata (initiated Wed Nov 08 22:34:17 CET 2017)
08.11.2017 22:34:17 - Resolving Executable Test Suite dependencies
08.11.2017 22:34:17 - Preparing 2 Test Task:
08.11.2017 22:34:17 -  TestTask 1 (ee8670d4-f254-4f79-97bb-c13abd9274e2)
08.11.2017 22:34:17 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'LAZY.e3500038-e37c-4dcf-806c-6bc82d585b3b'
08.11.2017 22:34:17 -  with parameters: 
08.11.2017 22:34:17 - etf.testcases = *
08.11.2017 22:34:17 -  TestTask 2 (4ee441db-f61e-4086-abdc-2ff23d7e21fb)
08.11.2017 22:34:17 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119 (EID: ec7323d5-d8f0-4cfe-b23a-b826df86d58c, V: 0.2.4 )'
08.11.2017 22:34:17 -  with parameters: 
08.11.2017 22:34:17 - etf.testcases = *
08.11.2017 22:34:17 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
08.11.2017 22:34:17 - Setting state to CREATED
08.11.2017 22:34:17 - Changed state from CREATED to INITIALIZING
08.11.2017 22:34:17 - Starting TestRun.4e0a10bb-3fd7-4043-8743-074169066b8a at 2017-11-08T22:34:19+01:00
08.11.2017 22:34:19 - Changed state from INITIALIZING to INITIALIZED
08.11.2017 22:34:19 - TestRunTask initialized
08.11.2017 22:34:19 - Creating new tests databases to speed up tests.
08.11.2017 22:34:19 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
08.11.2017 22:34:19 - Optimizing last database etf-tdb-682713c8-ad54-4e8c-b108-46d11b975606-0 
08.11.2017 22:34:19 - Import completed
08.11.2017 22:34:19 - Validation ended with 0 error(s)
08.11.2017 22:34:19 - Compiling test script
08.11.2017 22:34:19 - Starting XQuery tests
08.11.2017 22:34:19 - "Testing 1 records"
08.11.2017 22:34:19 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/xml/ets-md-xml-bsxets.xml"
08.11.2017 22:34:19 - "Statistics table: 0 ms"
08.11.2017 22:34:19 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' started"
08.11.2017 22:34:19 - "Test Case 'Schema validation' started"
08.11.2017 22:34:22 - "Validating file GetRecordByIdResponse.xml: 2403 ms"
08.11.2017 22:34:22 - "Test Assertion 'md-xml.a.1: Validate XML documents': FAILED - 2403 ms"
08.11.2017 22:34:22 - "Test Case 'Schema validation' finished: FAILED"
08.11.2017 22:34:22 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' finished: FAILED"
08.11.2017 22:34:22 - Releasing resources
08.11.2017 22:34:22 - TestRunTask initialized
08.11.2017 22:34:22 - Recreating new tests databases as the Test Object has changed!
08.11.2017 22:34:22 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
08.11.2017 22:34:22 - Optimizing last database etf-tdb-682713c8-ad54-4e8c-b108-46d11b975606-0 
08.11.2017 22:34:22 - Import completed
08.11.2017 22:34:22 - Validation ended with 0 error(s)
08.11.2017 22:34:22 - Compiling test script
08.11.2017 22:34:22 - Starting XQuery tests
08.11.2017 22:34:23 - "Testing 1 records"
08.11.2017 22:34:23 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/iso/ets-md-iso-bsxets.xml"
08.11.2017 22:34:23 - "Statistics table: 1 ms"
08.11.2017 22:34:23 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' started"
08.11.2017 22:34:23 - "Test Case 'Common tests' started"
08.11.2017 22:34:23 - "Test Assertion 'md-iso.a.1: Title': PASSED - 0 ms"
08.11.2017 22:34:23 - "Test Assertion 'md-iso.a.2: Abstract': PASSED - 0 ms"
08.11.2017 22:34:23 - "Test Assertion 'md-iso.a.3: Access and use conditions': FAILED - 0 ms"
08.11.2017 22:34:23 - "Test Assertion 'md-iso.a.4: Public access': FAILED - 1 ms"
08.11.2017 22:34:23 - "Test Assertion 'md-iso.a.5: Specification': PASSED - 0 ms"
08.11.2017 22:34:23 - "Test Assertion 'md-iso.a.6: Language': FAILED - 0 ms"
08.11.2017 22:34:23 - "Test Assertion 'md-iso.a.7: Metadata contact': PASSED - 0 ms"
08.11.2017 22:34:23 - "Test Assertion 'md-iso.a.8: Metadata contact role': FAILED - 0 ms"
08.11.2017 22:34:23 - "Test Assertion 'md-iso.a.9: Resource creation date': PASSED - 0 ms"
08.11.2017 22:34:23 - "Test Assertion 'md-iso.a.10: Responsible party contact info': FAILED - 0 ms"
08.11.2017 22:34:23 - "Test Assertion 'md-iso.a.11: Responsible party role': FAILED - 0 ms"
08.11.2017 22:34:23 - "Test Case 'Common tests' finished: FAILED"
08.11.2017 22:34:23 - "Test Case 'Hierarchy level' started"
08.11.2017 22:34:23 - "Test Assertion 'md-iso.b.1: Hierarchy': PASSED - 0 ms"
08.11.2017 22:34:23 - "Test Case 'Hierarchy level' finished: PASSED"
08.11.2017 22:34:23 - "Test Case 'Dataset (series) tests' started"
08.11.2017 22:34:23 - "Test Assertion 'md-iso.c.1: Dataset identification': PASSED - 0 ms"
08.11.2017 22:34:23 - "Test Assertion 'md-iso.c.2: Dataset language': PASSED - 0 ms"
08.11.2017 22:34:23 - "Test Assertion 'md-iso.c.3: Dataset linkage': PASSED - 0 ms"
08.11.2017 22:34:23 - "Test Assertion 'md-iso.c.4: Dataset conformity': PASSED - 0 ms"
08.11.2017 22:34:23 - "Test Assertion 'md-iso.c.5: Dataset topic': PASSED - 0 ms"
08.11.2017 22:34:23 - "Test Assertion 'md-iso.c.6: Dataset geographic Bounding box': PASSED - 0 ms"
08.11.2017 22:34:23 - "Test Assertion 'md-iso.c.7: Dataset lineage': PASSED - 0 ms"
08.11.2017 22:34:23 - "Test Case 'Dataset (series) tests' finished: PASSED"
08.11.2017 22:34:23 - "Test Case 'Service tests' started"
08.11.2017 22:34:23 - "Test Assertion 'md-iso.d.1: Service type': FAILED - 1 ms"
08.11.2017 22:34:23 - "Checking URL: 'https://zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get'"
08.11.2017 22:34:23 - "Test Assertion 'md-iso.d.2: Service linkage': PASSED_MANUAL - 197 ms"
08.11.2017 22:34:23 - "Checking URL: 'https://bolegweb.geof.unizg.hr/pycsw/inspire-testing?service=CSW&amp;version=2.0.2&amp;request=GetRecordById&amp;outputschema=http://www.isotc211.org/2005/gmd&amp;id=urn:uuid:a7eaf987-be28-4500-bad7-a6f3fc84d704-inspire_AdministrativeUnits_download:AdministrativeBoundary'"
08.11.2017 22:34:23 - "Exception: handshake alert:  unrecognized_name URL: https://bolegweb.geof.unizg.hr/pycsw/inspire-testing?service=CSW&amp;version=2.0.2&amp;request=GetRecordById&amp;outputschema=http://www.isotc211.org/2005/gmd&amp;id=urn:uuid:a7eaf987-be28-4500-bad7-a6f3fc84d704-inspire_AdministrativeUnits_download:AdministrativeBoundary"
08.11.2017 22:34:23 - "Checking URL: 'https://bolegweb.geof.unizg.hr/pycsw/inspire-testing?service=CSW&amp;version=2.0.2&amp;request=GetRecordById&amp;outputschema=http://www.isotc211.org/2005/gmd&amp;id=urn:uuid:a7eaf987-be28-4500-bad7-a6f3fc84d704-inspire_AdministrativeUnits_download:AdministrativeUnit'"
08.11.2017 22:34:23 - "Exception: handshake alert:  unrecognized_name URL: https://bolegweb.geof.unizg.hr/pycsw/inspire-testing?service=CSW&amp;version=2.0.2&amp;request=GetRecordById&amp;outputschema=http://www.isotc211.org/2005/gmd&amp;id=urn:uuid:a7eaf987-be28-4500-bad7-a6f3fc84d704-inspire_AdministrativeUnits_download:AdministrativeUnit"
08.11.2017 22:34:23 - "Test Assertion 'md-iso.d.3: Coupled resource': FAILED - 113 ms"
08.11.2017 22:34:23 - "Test Case 'Service tests' finished: FAILED"
08.11.2017 22:34:23 - "Test Case 'Keywords' started"
08.11.2017 22:34:23 - "Test Assertion 'md-iso.e.1: Keywords': PASSED - 0 ms"
08.11.2017 22:34:23 - "Test Case 'Keywords' finished: PASSED"
08.11.2017 22:34:23 - "Test Case 'Keywords - details' started"
08.11.2017 22:34:23 - "Test Assertion 'md-iso.f.1: Dataset keyword': PASSED - 0 ms"
08.11.2017 22:34:23 - "Checking URL: 'http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/SpatialDataServiceCategory.en.atom'"
08.11.2017 22:34:23 - "Test Assertion 'md-iso.f.2: Service keyword': PASSED - 10 ms"
08.11.2017 22:34:23 - "Test Assertion 'md-iso.f.3: Keywords in vocabulary grouped': PASSED - 0 ms"
08.11.2017 22:34:23 - "Test Assertion 'md-iso.f.4: Vocabulary information': PASSED - 0 ms"
08.11.2017 22:34:23 - "Test Case 'Keywords - details' finished: PASSED"
08.11.2017 22:34:23 - "Test Case 'Temporal extent' started"
08.11.2017 22:34:23 - "Test Assertion 'md-iso.g.1: Temporal extent': FAILED - 0 ms"
08.11.2017 22:34:23 - "Test Case 'Temporal extent' finished: FAILED"
08.11.2017 22:34:23 - "Test Case 'Temporal extent - details' started"
08.11.2017 22:34:23 - "Test Assertion 'md-iso.h.1: Temporal date': PASSED - 0 ms"
08.11.2017 22:34:23 - "Test Case 'Temporal extent - details' finished: PASSED"
08.11.2017 22:34:23 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' finished: FAILED"
08.11.2017 22:34:24 - Releasing resources
08.11.2017 22:34:24 - Changed state from INITIALIZED to RUNNING
08.11.2017 22:34:24 - Duration: 6sec
08.11.2017 22:34:24 - TestRun finished
08.11.2017 22:34:24 - Changed state from RUNNING to COMPLETED
