12.11.2017 18:53:23 - Preparing Test Run metadata (initiated Sun Nov 12 18:53:23 CET 2017)
12.11.2017 18:53:23 - Resolving Executable Test Suite dependencies
12.11.2017 18:53:23 - Preparing 2 Test Task:
12.11.2017 18:53:23 -  TestTask 1 (2a0faf37-fd91-49a3-b6c3-1494c5135a97)
12.11.2017 18:53:23 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'LAZY.e3500038-e37c-4dcf-806c-6bc82d585b3b'
12.11.2017 18:53:23 -  with parameters: 
12.11.2017 18:53:23 - etf.testcases = *
12.11.2017 18:53:23 -  TestTask 2 (b59292cc-72d6-4154-bb05-bbc6ef820625)
12.11.2017 18:53:23 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119 (EID: ec7323d5-d8f0-4cfe-b23a-b826df86d58c, V: 0.2.4 )'
12.11.2017 18:53:23 -  with parameters: 
12.11.2017 18:53:23 - etf.testcases = *
12.11.2017 18:53:23 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
12.11.2017 18:53:23 - Setting state to CREATED
12.11.2017 18:53:23 - Changed state from CREATED to INITIALIZING
12.11.2017 18:53:23 - Starting TestRun.aedd0d07-51e3-4876-9e90-9a60c2402264 at 2017-11-12T18:53:25+01:00
12.11.2017 18:53:25 - Changed state from INITIALIZING to INITIALIZED
12.11.2017 18:53:25 - TestRunTask initialized
12.11.2017 18:53:25 - Creating new tests databases to speed up tests.
12.11.2017 18:53:25 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.11.2017 18:53:25 - Optimizing last database etf-tdb-7bf5c705-c4f6-48a5-9ced-b9705196c150-0 
12.11.2017 18:53:25 - Import completed
12.11.2017 18:53:25 - Validation ended with 0 error(s)
12.11.2017 18:53:25 - Compiling test script
12.11.2017 18:53:25 - Starting XQuery tests
12.11.2017 18:53:25 - "Testing 1 records"
12.11.2017 18:53:25 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/xml/ets-md-xml-bsxets.xml"
12.11.2017 18:53:25 - "Statistics table: 1 ms"
12.11.2017 18:53:25 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' started"
12.11.2017 18:53:25 - "Test Case 'Schema validation' started"
12.11.2017 18:53:27 - "Validating file GetRecordByIdResponse.xml: 1861 ms"
12.11.2017 18:53:27 - "Test Assertion 'md-xml.a.1: Validate XML documents': FAILED - 1863 ms"
12.11.2017 18:53:27 - "Test Case 'Schema validation' finished: FAILED"
12.11.2017 18:53:27 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' finished: FAILED"
12.11.2017 18:53:27 - Releasing resources
12.11.2017 18:53:27 - TestRunTask initialized
12.11.2017 18:53:27 - Recreating new tests databases as the Test Object has changed!
12.11.2017 18:53:27 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.11.2017 18:53:27 - Optimizing last database etf-tdb-7bf5c705-c4f6-48a5-9ced-b9705196c150-0 
12.11.2017 18:53:27 - Import completed
12.11.2017 18:53:28 - Validation ended with 0 error(s)
12.11.2017 18:53:28 - Compiling test script
12.11.2017 18:53:28 - Starting XQuery tests
12.11.2017 18:53:28 - "Testing 1 records"
12.11.2017 18:53:28 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/iso/ets-md-iso-bsxets.xml"
12.11.2017 18:53:28 - "Statistics table: 1 ms"
12.11.2017 18:53:28 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' started"
12.11.2017 18:53:28 - "Test Case 'Common tests' started"
12.11.2017 18:53:28 - "Test Assertion 'md-iso.a.1: Title': PASSED - 1 ms"
12.11.2017 18:53:28 - "Test Assertion 'md-iso.a.2: Abstract': PASSED - 0 ms"
12.11.2017 18:53:28 - "Test Assertion 'md-iso.a.3: Access and use conditions': FAILED - 0 ms"
12.11.2017 18:53:28 - "Test Assertion 'md-iso.a.4: Public access': FAILED - 1 ms"
12.11.2017 18:53:28 - "Test Assertion 'md-iso.a.5: Specification': PASSED - 0 ms"
12.11.2017 18:53:28 - "Test Assertion 'md-iso.a.6: Language': FAILED - 0 ms"
12.11.2017 18:53:28 - "Test Assertion 'md-iso.a.7: Metadata contact': PASSED - 1 ms"
12.11.2017 18:53:28 - "Test Assertion 'md-iso.a.8: Metadata contact role': PASSED - 0 ms"
12.11.2017 18:53:28 - "Test Assertion 'md-iso.a.9: Resource creation date': PASSED - 0 ms"
12.11.2017 18:53:28 - "Test Assertion 'md-iso.a.10: Responsible party contact info': FAILED - 0 ms"
12.11.2017 18:53:28 - "Test Assertion 'md-iso.a.11: Responsible party role': FAILED - 0 ms"
12.11.2017 18:53:28 - "Test Case 'Common tests' finished: FAILED"
12.11.2017 18:53:28 - "Test Case 'Hierarchy level' started"
12.11.2017 18:53:28 - "Test Assertion 'md-iso.b.1: Hierarchy': PASSED - 0 ms"
12.11.2017 18:53:28 - "Test Case 'Hierarchy level' finished: PASSED"
12.11.2017 18:53:28 - "Test Case 'Dataset (series) tests' started"
12.11.2017 18:53:28 - "Test Assertion 'md-iso.c.1: Dataset identification': PASSED - 0 ms"
12.11.2017 18:53:28 - "Test Assertion 'md-iso.c.2: Dataset language': PASSED - 0 ms"
12.11.2017 18:53:28 - "Test Assertion 'md-iso.c.3: Dataset linkage': PASSED - 0 ms"
12.11.2017 18:53:28 - "Test Assertion 'md-iso.c.4: Dataset conformity': PASSED - 0 ms"
12.11.2017 18:53:28 - "Test Assertion 'md-iso.c.5: Dataset topic': PASSED - 0 ms"
12.11.2017 18:53:28 - "Test Assertion 'md-iso.c.6: Dataset geographic Bounding box': PASSED - 0 ms"
12.11.2017 18:53:28 - "Test Assertion 'md-iso.c.7: Dataset lineage': PASSED - 0 ms"
12.11.2017 18:53:28 - "Test Case 'Dataset (series) tests' finished: PASSED"
12.11.2017 18:53:28 - "Test Case 'Service tests' started"
12.11.2017 18:53:28 - "Test Assertion 'md-iso.d.1: Service type': FAILED - 0 ms"
12.11.2017 18:53:28 - "Checking URL: 'https://test-zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get'"
12.11.2017 18:53:29 - "Test Assertion 'md-iso.d.2: Service linkage': PASSED_MANUAL - 1177 ms"
12.11.2017 18:53:29 - "Checking URL: 'https://bolegweb.geof.unizg.hr/pycsw/inspire-testing?service=CSW&amp;version=2.0.2&amp;request=GetRecordById&amp;outputschema=http://www.isotc211.org/2005/gmd&amp;id=urn:uuid:b6859321-bd80-4b49-a816-c8746520e4d2-au:AdministrativeBoundary'"
12.11.2017 18:53:29 - "Exception: handshake alert:  unrecognized_name URL: https://bolegweb.geof.unizg.hr/pycsw/inspire-testing?service=CSW&amp;version=2.0.2&amp;request=GetRecordById&amp;outputschema=http://www.isotc211.org/2005/gmd&amp;id=urn:uuid:b6859321-bd80-4b49-a816-c8746520e4d2-au:AdministrativeBoundary"
12.11.2017 18:53:29 - "Checking URL: 'https://bolegweb.geof.unizg.hr/pycsw/inspire-testing?service=CSW&amp;version=2.0.2&amp;request=GetRecordById&amp;outputschema=http://www.isotc211.org/2005/gmd&amp;id=urn:uuid:b6859321-bd80-4b49-a816-c8746520e4d2-au:AdministrativeUnit'"
12.11.2017 18:53:29 - "Exception: handshake alert:  unrecognized_name URL: https://bolegweb.geof.unizg.hr/pycsw/inspire-testing?service=CSW&amp;version=2.0.2&amp;request=GetRecordById&amp;outputschema=http://www.isotc211.org/2005/gmd&amp;id=urn:uuid:b6859321-bd80-4b49-a816-c8746520e4d2-au:AdministrativeUnit"
12.11.2017 18:53:29 - "Test Assertion 'md-iso.d.3: Coupled resource': FAILED - 141 ms"
12.11.2017 18:53:29 - "Test Case 'Service tests' finished: FAILED"
12.11.2017 18:53:29 - "Test Case 'Keywords' started"
12.11.2017 18:53:29 - "Test Assertion 'md-iso.e.1: Keywords': PASSED - 0 ms"
12.11.2017 18:53:29 - "Test Case 'Keywords' finished: PASSED"
12.11.2017 18:53:29 - "Test Case 'Keywords - details' started"
12.11.2017 18:53:29 - "Test Assertion 'md-iso.f.1: Dataset keyword': PASSED - 0 ms"
12.11.2017 18:53:29 - "Checking URL: 'http://inspire.ec.europa.eu/metadata-codelist/SpatialDataServiceCategory/SpatialDataServiceCategory.en.atom'"
12.11.2017 18:53:29 - "Test Assertion 'md-iso.f.2: Service keyword': PASSED - 16 ms"
12.11.2017 18:53:29 - "Test Assertion 'md-iso.f.3: Keywords in vocabulary grouped': PASSED - 0 ms"
12.11.2017 18:53:29 - "Test Assertion 'md-iso.f.4: Vocabulary information': PASSED - 0 ms"
12.11.2017 18:53:29 - "Test Case 'Keywords - details' finished: PASSED"
12.11.2017 18:53:29 - "Test Case 'Temporal extent' started"
12.11.2017 18:53:29 - "Test Assertion 'md-iso.g.1: Temporal extent': FAILED - 0 ms"
12.11.2017 18:53:29 - "Test Case 'Temporal extent' finished: FAILED"
12.11.2017 18:53:29 - "Test Case 'Temporal extent - details' started"
12.11.2017 18:53:29 - "Test Assertion 'md-iso.h.1: Temporal date': PASSED - 0 ms"
12.11.2017 18:53:29 - "Test Case 'Temporal extent - details' finished: PASSED"
12.11.2017 18:53:29 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' finished: FAILED"
12.11.2017 18:53:30 - Releasing resources
12.11.2017 18:53:30 - Changed state from INITIALIZED to RUNNING
12.11.2017 18:53:30 - Duration: 7sec
12.11.2017 18:53:30 - TestRun finished
12.11.2017 18:53:30 - Changed state from RUNNING to COMPLETED
