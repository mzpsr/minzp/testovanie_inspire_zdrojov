12.11.2017 18:55:44 - Preparing Test Run metadata (initiated Sun Nov 12 18:55:44 CET 2017)
12.11.2017 18:55:44 - Resolving Executable Test Suite dependencies
12.11.2017 18:55:44 - Preparing 2 Test Task:
12.11.2017 18:55:44 -  TestTask 1 (fe9177cd-f757-4216-89c6-70351c4e3ffd)
12.11.2017 18:55:44 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'LAZY.e3500038-e37c-4dcf-806c-6bc82d585b3b'
12.11.2017 18:55:44 -  with parameters: 
12.11.2017 18:55:44 - etf.testcases = *
12.11.2017 18:55:44 -  TestTask 2 (b7a39fff-be48-4da1-bb21-9b19acaf9e28)
12.11.2017 18:55:44 -  will perform tests on Test Object 'GetRecordByIdResponse.xml' by using Executable Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119 (EID: ec7323d5-d8f0-4cfe-b23a-b826df86d58c, V: 0.2.4 )'
12.11.2017 18:55:44 -  with parameters: 
12.11.2017 18:55:44 - etf.testcases = *
12.11.2017 18:55:44 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
12.11.2017 18:55:44 - Setting state to CREATED
12.11.2017 18:55:44 - Changed state from CREATED to INITIALIZING
12.11.2017 18:55:44 - Starting TestRun.582c862e-5a1f-41c7-9304-ab0ab3d94d7a at 2017-11-12T18:55:45+01:00
12.11.2017 18:55:45 - Changed state from INITIALIZING to INITIALIZED
12.11.2017 18:55:45 - TestRunTask initialized
12.11.2017 18:55:45 - Creating new tests databases to speed up tests.
12.11.2017 18:55:45 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.11.2017 18:55:45 - Optimizing last database etf-tdb-16adc614-f845-4bb2-a9f0-38ad57d9a783-0 
12.11.2017 18:55:46 - Import completed
12.11.2017 18:55:46 - Validation ended with 0 error(s)
12.11.2017 18:55:46 - Compiling test script
12.11.2017 18:55:46 - Starting XQuery tests
12.11.2017 18:55:46 - "Testing 1 records"
12.11.2017 18:55:46 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/xml/ets-md-xml-bsxets.xml"
12.11.2017 18:55:46 - "Statistics table: 1 ms"
12.11.2017 18:55:46 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' started"
12.11.2017 18:55:46 - "Test Case 'Schema validation' started"
12.11.2017 18:55:48 - "Validating file GetRecordByIdResponse.xml: 1975 ms"
12.11.2017 18:55:48 - "Test Assertion 'md-xml.a.1: Validate XML documents': FAILED - 1976 ms"
12.11.2017 18:55:48 - "Test Case 'Schema validation' finished: FAILED"
12.11.2017 18:55:48 - "Test Suite 'Conformance class: XML encoding of ISO 19115/19119 metadata' finished: FAILED"
12.11.2017 18:55:48 - Releasing resources
12.11.2017 18:55:48 - TestRunTask initialized
12.11.2017 18:55:48 - Recreating new tests databases as the Test Object has changed!
12.11.2017 18:55:48 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
12.11.2017 18:55:49 - Optimizing last database etf-tdb-16adc614-f845-4bb2-a9f0-38ad57d9a783-0 
12.11.2017 18:55:49 - Import completed
12.11.2017 18:55:49 - Validation ended with 0 error(s)
12.11.2017 18:55:49 - Compiling test script
12.11.2017 18:55:49 - Starting XQuery tests
12.11.2017 18:55:49 - "Testing 1 records"
12.11.2017 18:55:49 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/metadata/iso/ets-md-iso-bsxets.xml"
12.11.2017 18:55:49 - "Statistics table: 0 ms"
12.11.2017 18:55:49 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' started"
12.11.2017 18:55:49 - "Test Case 'Common tests' started"
12.11.2017 18:55:49 - "Test Assertion 'md-iso.a.1: Title': PASSED - 0 ms"
12.11.2017 18:55:49 - "Test Assertion 'md-iso.a.2: Abstract': FAILED - 1 ms"
12.11.2017 18:55:49 - "Test Assertion 'md-iso.a.3: Access and use conditions': FAILED - 0 ms"
12.11.2017 18:55:49 - "Test Assertion 'md-iso.a.4: Public access': FAILED - 0 ms"
12.11.2017 18:55:49 - "Test Assertion 'md-iso.a.5: Specification': PASSED - 0 ms"
12.11.2017 18:55:49 - "Test Assertion 'md-iso.a.6: Language': FAILED - 0 ms"
12.11.2017 18:55:49 - "Test Assertion 'md-iso.a.7: Metadata contact': PASSED - 0 ms"
12.11.2017 18:55:49 - "Test Assertion 'md-iso.a.8: Metadata contact role': PASSED - 0 ms"
12.11.2017 18:55:49 - "Test Assertion 'md-iso.a.9: Resource creation date': PASSED - 0 ms"
12.11.2017 18:55:49 - "Test Assertion 'md-iso.a.10: Responsible party contact info': FAILED - 0 ms"
12.11.2017 18:55:49 - "Test Assertion 'md-iso.a.11: Responsible party role': FAILED - 0 ms"
12.11.2017 18:55:49 - "Test Case 'Common tests' finished: FAILED"
12.11.2017 18:55:49 - "Test Case 'Hierarchy level' started"
12.11.2017 18:55:49 - "Test Assertion 'md-iso.b.1: Hierarchy': PASSED - 1 ms"
12.11.2017 18:55:49 - "Test Case 'Hierarchy level' finished: PASSED"
12.11.2017 18:55:49 - "Test Case 'Dataset (series) tests' started"
12.11.2017 18:55:49 - "Test Assertion 'md-iso.c.1: Dataset identification': FAILED - 0 ms"
12.11.2017 18:55:49 - "Test Assertion 'md-iso.c.2: Dataset language': FAILED - 0 ms"
12.11.2017 18:55:49 - "Checking URL: 'https://test-zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get'"
12.11.2017 18:55:49 - "Checking URL: 'https://test-zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?typename=au%3AAdministrativeBoundary&amp;version=1.1.0&amp;request=GetFeature&amp;service=WFS'"
12.11.2017 18:56:19 - "Exception: Timeout exceeded. URL: https://test-zbgisws.skgeodesy.sk/inspire_administrative_units_wfs/service.svc/get?typename=au%3AAdministrativeBoundary&amp;version=1.1.0&amp;request=GetFeature&amp;service=WFS"
12.11.2017 18:56:19 - "Test Assertion 'md-iso.c.3: Dataset linkage': FAILED - 30271 ms"
12.11.2017 18:56:19 - "Test Assertion 'md-iso.c.4: Dataset conformity': FAILED - 1 ms"
12.11.2017 18:56:19 - "Test Assertion 'md-iso.c.5: Dataset topic': FAILED - 0 ms"
12.11.2017 18:56:19 - "Test Assertion 'md-iso.c.6: Dataset geographic Bounding box': FAILED - 0 ms"
12.11.2017 18:56:19 - "Test Assertion 'md-iso.c.7: Dataset lineage': FAILED - 0 ms"
12.11.2017 18:56:19 - "Test Case 'Dataset (series) tests' finished: FAILED"
12.11.2017 18:56:19 - "Test Case 'Service tests' started"
12.11.2017 18:56:19 - "Test Assertion 'md-iso.d.1: Service type': PASSED - 0 ms"
12.11.2017 18:56:19 - "Test Assertion 'md-iso.d.2: Service linkage': PASSED - 0 ms"
12.11.2017 18:56:19 - "Test Assertion 'md-iso.d.3: Coupled resource': PASSED - 0 ms"
12.11.2017 18:56:19 - "Test Case 'Service tests' finished: PASSED"
12.11.2017 18:56:19 - "Test Case 'Keywords' started"
12.11.2017 18:56:19 - "Test Assertion 'md-iso.e.1: Keywords': FAILED - 0 ms"
12.11.2017 18:56:19 - "Test Case 'Keywords' finished: FAILED"
12.11.2017 18:56:19 - "Test Case 'Keywords - details' started"
12.11.2017 18:56:19 - "Test Assertion 'md-iso.f.1: Dataset keyword': PASSED - 0 ms"
12.11.2017 18:56:19 - "Test Assertion 'md-iso.f.2: Service keyword': PASSED - 0 ms"
12.11.2017 18:56:19 - "Test Assertion 'md-iso.f.3: Keywords in vocabulary grouped': PASSED - 0 ms"
12.11.2017 18:56:19 - "Test Assertion 'md-iso.f.4: Vocabulary information': PASSED - 0 ms"
12.11.2017 18:56:19 - "Test Case 'Keywords - details' finished: PASSED"
12.11.2017 18:56:19 - "Test Case 'Temporal extent' started"
12.11.2017 18:56:19 - "Test Assertion 'md-iso.g.1: Temporal extent': FAILED - 0 ms"
12.11.2017 18:56:19 - "Test Case 'Temporal extent' finished: FAILED"
12.11.2017 18:56:19 - "Test Case 'Temporal extent - details' started"
12.11.2017 18:56:19 - "Test Assertion 'md-iso.h.1: Temporal date': PASSED - 0 ms"
12.11.2017 18:56:19 - "Test Case 'Temporal extent - details' finished: PASSED"
12.11.2017 18:56:19 - "Test Suite 'Conformance class: INSPIRE Profile based on EN ISO 19115 and EN ISO 19119' finished: FAILED"
12.11.2017 18:56:20 - Releasing resources
12.11.2017 18:56:20 - Changed state from INITIALIZED to RUNNING
12.11.2017 18:56:21 - Duration: 36sec
12.11.2017 18:56:21 - TestRun finished
12.11.2017 18:56:21 - Changed state from RUNNING to COMPLETED
