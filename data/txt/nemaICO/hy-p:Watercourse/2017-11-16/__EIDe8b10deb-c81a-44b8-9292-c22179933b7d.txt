16.11.2017 00:42:13 - Preparing Test Run hy-p:Watercourse (initiated Thu Nov 16 00:42:13 CET 2017)
16.11.2017 00:42:13 - Resolving Executable Test Suite dependencies
16.11.2017 00:42:13 - Preparing 10 Test Task:
16.11.2017 00:42:13 -  TestTask 1 (24db124b-264e-4154-92ed-77357cc57a31)
16.11.2017 00:42:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.545f9e49-009b-4114-9333-7ca26413b5d4'
16.11.2017 00:42:13 -  with parameters: 
16.11.2017 00:42:13 - etf.testcases = *
16.11.2017 00:42:13 -  TestTask 2 (eb2d8298-e573-4063-baa0-6de8065f7741)
16.11.2017 00:42:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements (EID: 09820daf-62b2-4fa3-a95f-56a0d2b7c4d8, V: 0.2.3 )'
16.11.2017 00:42:13 -  with parameters: 
16.11.2017 00:42:13 - etf.testcases = *
16.11.2017 00:42:13 -  TestTask 3 (6940e541-6ec3-4356-bdbf-c29fb4b969b0)
16.11.2017 00:42:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.81b070d3-b17f-430b-abee-456268346912'
16.11.2017 00:42:13 -  with parameters: 
16.11.2017 00:42:13 - etf.testcases = *
16.11.2017 00:42:13 -  TestTask 4 (086b1d3a-7b19-46d5-9a59-0de0edcaaf0b)
16.11.2017 00:42:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Application schema, Hydrography - Physical Waters (EID: 45133c90-1929-405c-867d-9648b0620bf7, V: 0.2.1 )'
16.11.2017 00:42:13 -  with parameters: 
16.11.2017 00:42:13 - etf.testcases = *
16.11.2017 00:42:13 -  TestTask 5 (162fa803-ebca-47f2-b84d-06b006a9b4e0)
16.11.2017 00:42:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.61070ae8-13cb-4303-a340-72c8b877b00a'
16.11.2017 00:42:13 -  with parameters: 
16.11.2017 00:42:13 - etf.testcases = *
16.11.2017 00:42:13 -  TestTask 6 (18a4a7de-a716-4885-be7c-547817d0fe39)
16.11.2017 00:42:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Data consistency, Hydrography (EID: d0b58f38-98ae-43a8-a787-9a5084c60267, V: 0.2.2 )'
16.11.2017 00:42:13 -  with parameters: 
16.11.2017 00:42:13 - etf.testcases = *
16.11.2017 00:42:13 -  TestTask 7 (d0d820bb-f882-4b32-9a91-e1dfcfb9510c)
16.11.2017 00:42:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.499937ea-0590-42d2-bd7a-1cafff35ecdb'
16.11.2017 00:42:13 -  with parameters: 
16.11.2017 00:42:13 - etf.testcases = *
16.11.2017 00:42:13 -  TestTask 8 (a3ecce96-cbd0-41ce-a44f-add6555f1253)
16.11.2017 00:42:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Information accessibility, Hydrography (EID: 893b7541-c9cb-4e0a-9f84-5d55cad1866c, V: 0.2.1 )'
16.11.2017 00:42:13 -  with parameters: 
16.11.2017 00:42:13 - etf.testcases = *
16.11.2017 00:42:13 -  TestTask 9 (c14e4780-6004-4b82-aca5-0c9a6973cc28)
16.11.2017 00:42:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'LAZY.63f586f0-080c-493b-8ca2-9919427440cc'
16.11.2017 00:42:13 -  with parameters: 
16.11.2017 00:42:13 - etf.testcases = *
16.11.2017 00:42:13 -  TestTask 10 (ac3ebfda-230c-4860-b15a-22a5ffbaaf7f)
16.11.2017 00:42:13 -  will perform tests on Test Object 'get.xml' by using Executable Test Suite 'Conformance class: Reference systems, Hydrography (EID: 122b2f38-302f-4271-9653-69cf86fcb5c4, V: 0.2.1 )'
16.11.2017 00:42:13 -  with parameters: 
16.11.2017 00:42:13 - etf.testcases = *
16.11.2017 00:42:13 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
16.11.2017 00:42:13 - Setting state to CREATED
16.11.2017 00:42:13 - Changed state from CREATED to INITIALIZING
16.11.2017 00:42:15 - Starting TestRun.e8b10deb-c81a-44b8-9292-c22179933b7d at 2017-11-16T00:42:17+01:00
16.11.2017 00:42:17 - Changed state from INITIALIZING to INITIALIZED
16.11.2017 00:42:17 - TestRunTask initialized
16.11.2017 00:42:17 - Creating new tests databases to speed up tests.
16.11.2017 00:42:17 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
16.11.2017 00:42:17 - Optimizing last database etf-tdb-d30e5b7f-c12b-4d63-9873-bad31412131c-0 
16.11.2017 00:42:17 - Import completed
16.11.2017 00:42:18 - Validation ended with 0 error(s)
16.11.2017 00:42:18 - Compiling test script
16.11.2017 00:42:18 - Starting XQuery tests
16.11.2017 00:42:18 - "Testing 0 features"
16.11.2017 00:42:18 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-encoding/inspire-gml/ets-inspire-gml-bsxets.xml"
16.11.2017 00:42:18 - "Statistics table: 0 ms"
16.11.2017 00:42:18 - "Test Suite 'Conformance class: INSPIRE GML encoding' started"
16.11.2017 00:42:18 - "Test Case 'Basic tests' started"
16.11.2017 00:42:18 - "Test Assertion 'gml.a.1: Errors loading the XML documents': PASSED - 0 ms"
16.11.2017 00:42:18 - "Test Assertion 'gml.a.2: Document root element': PASSED - 0 ms"
16.11.2017 00:42:18 - "Test Assertion 'gml.a.3: Character encoding': NOT_APPLICABLE"
16.11.2017 00:42:18 - "Test Case 'Basic tests' finished: PASSED"
16.11.2017 00:42:18 - "Test Suite 'Conformance class: INSPIRE GML encoding' finished: PASSED"
16.11.2017 00:42:19 - Releasing resources
16.11.2017 00:42:19 - TestRunTask initialized
16.11.2017 00:42:19 - Recreating new tests databases as the Test Object has changed!
16.11.2017 00:42:19 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
16.11.2017 00:42:19 - Optimizing last database etf-tdb-d30e5b7f-c12b-4d63-9873-bad31412131c-0 
16.11.2017 00:42:19 - Import completed
16.11.2017 00:42:19 - Validation ended with 0 error(s)
16.11.2017 00:42:19 - Compiling test script
16.11.2017 00:42:19 - Starting XQuery tests
16.11.2017 00:42:19 - "Testing 0 features"
16.11.2017 00:42:19 - "Indexing features (parsing errors: 0): 0 ms"
16.11.2017 00:42:19 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/schemas/ets-schemas-bsxets.xml"
16.11.2017 00:42:19 - "Statistics table: 0 ms"
16.11.2017 00:42:19 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' started"
16.11.2017 00:42:19 - "Test Case 'Schema' started"
16.11.2017 00:42:19 - "Test Assertion 'gmlas.a.1: Mapping of source data to INSPIRE': PASSED_MANUAL"
16.11.2017 00:42:19 - "Test Assertion 'gmlas.a.2: Modelling of additional spatial object types': PASSED_MANUAL"
16.11.2017 00:42:19 - "Test Case 'Schema' finished: PASSED_MANUAL"
16.11.2017 00:42:19 - "Test Case 'Schema validation' started"
16.11.2017 00:42:19 - "Test Assertion 'gmlas.b.1: xsi:schemaLocation attribute': PASSED - 0 ms"
16.11.2017 00:42:19 - "Validating get.xml"
16.11.2017 00:42:21 - "Duration: 2225 ms. Errors: 1."
16.11.2017 00:42:21 - "Test Assertion 'gmlas.b.2: validate XML documents': FAILED - 2226 ms"
16.11.2017 00:42:21 - "Test Case 'Schema validation' finished: FAILED"
16.11.2017 00:42:21 - "Test Case 'GML model' started"
16.11.2017 00:42:21 - "Test Assertion 'gmlas.c.1: Consistency with the GML model': PASSED - 0 ms"
16.11.2017 00:42:21 - "Test Assertion 'gmlas.c.2: nilReason attributes require xsi:nil=true': PASSED - 0 ms"
16.11.2017 00:42:21 - "Test Assertion 'gmlas.c.3: nilReason values': PASSED - 0 ms"
16.11.2017 00:42:21 - "Test Case 'GML model' finished: PASSED"
16.11.2017 00:42:21 - "Test Case 'Simple features' started"
16.11.2017 00:42:21 - "Test Assertion 'gmlas.d.1: No spatial topology objects': PASSED - 0 ms"
16.11.2017 00:42:21 - "Test Assertion 'gmlas.d.2: No non-linear interpolation': PASSED - 0 ms"
16.11.2017 00:42:21 - "Test Assertion 'gmlas.d.3: Surface geometry elements': PASSED - 0 ms"
16.11.2017 00:42:21 - "Test Assertion 'gmlas.d.4: No non-planar interpolation': PASSED - 0 ms"
16.11.2017 00:42:21 - "Test Assertion 'gmlas.d.5: Geometry elements': PASSED - 0 ms"
16.11.2017 00:42:21 - "Test Assertion 'gmlas.d.6: Point coordinates in gml:pos': PASSED - 0 ms"
16.11.2017 00:42:21 - "Test Assertion 'gmlas.d.7: Curve/Surface coordinates in gml:posList': PASSED - 0 ms"
16.11.2017 00:42:21 - "Test Assertion 'gmlas.d.8: No array property elements': PASSED - 0 ms"
16.11.2017 00:42:21 - "Test Assertion 'gmlas.d.9: 1, 2 or 3 coordinate dimensions': PASSED - 0 ms"
16.11.2017 00:42:21 - "Test Assertion 'gmlas.d.10: Validate geometries (1)': PASSED - 0 ms"
16.11.2017 00:42:21 - "Test Assertion 'gmlas.d.11: Validate geometries (2)': PASSED - 0 ms"
16.11.2017 00:42:21 - "Test Case 'Simple features' finished: PASSED"
16.11.2017 00:42:21 - "Test Case 'Code list values in basic data types' started"
16.11.2017 00:42:21 - "Test Assertion 'gmlas.e.1: GrammaticalNumber attributes': PASSED - 8 ms"
16.11.2017 00:42:21 - "Test Assertion 'gmlas.e.2: GrammaticalGender attributes': PASSED - 4 ms"
16.11.2017 00:42:21 - "Test Assertion 'gmlas.e.3: NameStatus attributes': PASSED - 4 ms"
16.11.2017 00:42:21 - "Test Assertion 'gmlas.e.4: Nativeness attributes': PASSED - 4 ms"
16.11.2017 00:42:21 - "Test Case 'Code list values in basic data types' finished: PASSED"
16.11.2017 00:42:21 - "Test Case 'Constraints' started"
16.11.2017 00:42:21 - "Test Assertion 'gmlas.f.1: At least one of the two attributes pronunciationSoundLink and pronunciationIPA shall not be void': PASSED - 0 ms"
16.11.2017 00:42:21 - "Test Case 'Constraints' finished: PASSED"
16.11.2017 00:42:21 - "Test Suite 'Conformance class: INSPIRE GML application schemas, General requirements' finished: FAILED"
16.11.2017 00:42:23 - Releasing resources
16.11.2017 00:42:23 - TestRunTask initialized
16.11.2017 00:42:23 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
16.11.2017 00:42:23 - Validation ended with 0 error(s)
16.11.2017 00:42:23 - Compiling test script
16.11.2017 00:42:23 - Starting XQuery tests
16.11.2017 00:42:23 - "Testing 0 features"
16.11.2017 00:42:23 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-gml/ets-hy-gml-bsxets.xml"
16.11.2017 00:42:23 - "Statistics table: 0 ms"
16.11.2017 00:42:23 - "Test Suite 'Conformance class: GML application schemas, Hydrography' started"
16.11.2017 00:42:23 - "Test Case 'Basic test' started"
16.11.2017 00:42:23 - "Test Assertion 'hy-gml.a.1: Hydrographic feature in dataset': FAILED - 0 ms"
16.11.2017 00:42:23 - "Test Case 'Basic test' finished: FAILED"
16.11.2017 00:42:23 - "Test Suite 'Conformance class: GML application schemas, Hydrography' finished: FAILED"
16.11.2017 00:42:24 - Releasing resources
16.11.2017 00:42:24 - TestRunTask initialized
16.11.2017 00:42:24 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
16.11.2017 00:42:24 - Validation ended with 0 error(s)
16.11.2017 00:42:24 - Compiling test script
16.11.2017 00:42:24 - Starting XQuery tests
16.11.2017 00:42:24 - "Testing 0 features"
16.11.2017 00:42:24 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-p-as/ets-hy-p-as-bsxets.xml"
16.11.2017 00:42:24 - "Statistics table: 0 ms"
16.11.2017 00:42:24 - "Test Suite 'Conformance class: Application schema, Hydrography - Physical Waters' started"
16.11.2017 00:42:24 - "Test Case 'Code list values' started"
16.11.2017 00:42:24 - "Test Assertion 'hy-p-as.a.1: condition attributes': PASSED - 20 ms"
16.11.2017 00:42:24 - "Test Assertion 'hy-p-as.a.2: type attributes': PASSED - 16 ms"
16.11.2017 00:42:24 - "Test Assertion 'hy-p-as.a.3: waterLevelCategory attributes': PASSED - 38 ms"
16.11.2017 00:42:24 - "Test Assertion 'hy-p-as.a.4: composition attributes': PASSED - 10 ms"
16.11.2017 00:42:24 - "Test Assertion 'hy-p-as.a.5: persistence attributes': PASSED - 12 ms"
16.11.2017 00:42:24 - "Test Case 'Code list values' finished: PASSED"
16.11.2017 00:42:24 - "Test Case 'Geometry' started"
16.11.2017 00:42:24 - "Test Assertion 'hy-p-as.b.1: Level of detail': PASSED_MANUAL"
16.11.2017 00:42:24 - "Test Case 'Geometry' finished: PASSED_MANUAL"
16.11.2017 00:42:24 - "Test Case 'Identifiers and references' started"
16.11.2017 00:42:24 - "Test Assertion 'hy-p-as.c.1: Reuse of authoritative, pan-European identifiers': PASSED_MANUAL"
16.11.2017 00:42:24 - "Test Case 'Identifiers and references' finished: PASSED_MANUAL"
16.11.2017 00:42:24 - "Test Case 'Constraints' started"
16.11.2017 00:42:24 - "Test Assertion 'hy-p-as.d.1: A river basin may not be contained in any other basin': PASSED - 0 ms"
16.11.2017 00:42:24 - "Test Assertion 'hy-p-as.d.2: A standing water geometry may be a surface or point': PASSED - 0 ms"
16.11.2017 00:42:24 - "Test Assertion 'hy-p-as.d.3: A watercourse geometry may be a curve or surface': PASSED - 0 ms"
16.11.2017 00:42:24 - "Test Assertion 'hy-p-as.d.4: A condition attribute may be specified only for a man-made watercourse': PASSED - 0 ms"
16.11.2017 00:42:24 - "Test Assertion 'hy-p-as.d.5: Shores on either side of a watercourse shall be provided as separate Shore objects': PASSED_MANUAL"
16.11.2017 00:42:24 - "Test Case 'Constraints' finished: PASSED_MANUAL"
16.11.2017 00:42:24 - "Test Suite 'Conformance class: Application schema, Hydrography - Physical Waters' finished: PASSED_MANUAL"
16.11.2017 00:42:25 - Releasing resources
16.11.2017 00:42:25 - TestRunTask initialized
16.11.2017 00:42:25 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
16.11.2017 00:42:25 - Validation ended with 0 error(s)
16.11.2017 00:42:25 - Compiling test script
16.11.2017 00:42:25 - Starting XQuery tests
16.11.2017 00:42:25 - "Testing 0 features"
16.11.2017 00:42:25 - "Indexing features (parsing errors: 0): 1 ms"
16.11.2017 00:42:25 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/data-consistency/ets-data-consistency-bsxets.xml"
16.11.2017 00:42:25 - "Statistics table: 0 ms"
16.11.2017 00:42:25 - "Test Suite 'Conformance class: Data consistency, General requirements' started"
16.11.2017 00:42:25 - "Test Case 'Version consistency' started"
16.11.2017 00:42:25 - "Test Assertion 'dc.a.1: Version lifespan plausible': PASSED - 0 ms"
16.11.2017 00:42:25 - "Test Assertion 'dc.a.2: Unique identifier persistency': PASSED_MANUAL"
16.11.2017 00:42:25 - "Test Assertion 'dc.a.3: Spatial object type stable': PASSED_MANUAL"
16.11.2017 00:42:25 - "Test Case 'Version consistency' finished: PASSED_MANUAL"
16.11.2017 00:42:25 - "Test Case 'Temporal consistency' started"
16.11.2017 00:42:25 - "Test Assertion 'dc.b.1: Valid time plausible': PASSED - 0 ms"
16.11.2017 00:42:25 - "Test Case 'Temporal consistency' finished: PASSED"
16.11.2017 00:42:25 - "Test Suite 'Conformance class: Data consistency, General requirements' finished: PASSED_MANUAL"
16.11.2017 00:42:26 - Releasing resources
16.11.2017 00:42:26 - TestRunTask initialized
16.11.2017 00:42:26 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
16.11.2017 00:42:26 - Validation ended with 0 error(s)
16.11.2017 00:42:26 - Compiling test script
16.11.2017 00:42:26 - Starting XQuery tests
16.11.2017 00:42:26 - "Testing 0 features"
16.11.2017 00:42:26 - "Indexing features (parsing errors: 0): 0 ms"
16.11.2017 00:42:26 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-dc/ets-hy-dc-bsxets.xml"
16.11.2017 00:42:26 - "Statistics table: 0 ms"
16.11.2017 00:42:26 - "Test Suite 'Conformance class: Data consistency, Hydrography' started"
16.11.2017 00:42:26 - "Test Case 'Spatial consistency' started"
16.11.2017 00:42:26 - "Test Assertion 'hy-dc.a.1: Each Network geometry is within a physical water geometry': PASSED - 0 ms"
16.11.2017 00:42:26 - "Test Assertion 'hy-dc.a.2: Manual review': PASSED_MANUAL"
16.11.2017 00:42:26 - "Test Case 'Spatial consistency' finished: PASSED_MANUAL"
16.11.2017 00:42:26 - "Test Case 'Thematic consistency' started"
16.11.2017 00:42:26 - "Test Assertion 'hy-dc.b.1: Consistency with Water Framework Directive reporting': PASSED_MANUAL"
16.11.2017 00:42:26 - "Test Case 'Thematic consistency' finished: PASSED_MANUAL"
16.11.2017 00:42:26 - "Test Case 'Identifiers' started"
16.11.2017 00:42:26 - "Test Assertion 'hy-dc.c.1: Reusing authoritative, pan-European sources': PASSED_MANUAL"
16.11.2017 00:42:26 - "Test Assertion 'hy-dc.c.2: Consistency with Water Framework Directive reporting': PASSED_MANUAL"
16.11.2017 00:42:26 - "Test Case 'Identifiers' finished: PASSED_MANUAL"
16.11.2017 00:42:26 - "Test Suite 'Conformance class: Data consistency, Hydrography' finished: PASSED_MANUAL"
16.11.2017 00:42:27 - Releasing resources
16.11.2017 00:42:27 - TestRunTask initialized
16.11.2017 00:42:27 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
16.11.2017 00:42:27 - Validation ended with 0 error(s)
16.11.2017 00:42:27 - Compiling test script
16.11.2017 00:42:27 - Starting XQuery tests
16.11.2017 00:42:27 - "Testing 0 features"
16.11.2017 00:42:27 - "Indexing features (parsing errors: 0): 0 ms"
16.11.2017 00:42:27 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/information-accessibility/ets-information-accessibility-bsxets.xml"
16.11.2017 00:42:27 - "Statistics table: 1 ms"
16.11.2017 00:42:27 - "Test Suite 'Conformance class: Information accessibility, General requirements' started"
16.11.2017 00:42:27 - "Test Case 'Coordinate reference systems (CRS)' started"
16.11.2017 00:42:27 - "Test Assertion 'ia.a.1: CRS publicly accessible via HTTP': PASSED - 0 ms"
16.11.2017 00:42:27 - "Test Case 'Coordinate reference systems (CRS)' finished: PASSED"
16.11.2017 00:42:27 - "Test Suite 'Conformance class: Information accessibility, General requirements' finished: PASSED"
16.11.2017 00:42:27 - Releasing resources
16.11.2017 00:42:27 - TestRunTask initialized
16.11.2017 00:42:27 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
16.11.2017 00:42:27 - Validation ended with 0 error(s)
16.11.2017 00:42:27 - Compiling test script
16.11.2017 00:42:27 - Starting XQuery tests
16.11.2017 00:42:27 - "Testing 0 features"
16.11.2017 00:42:27 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-ia/ets-hy-ia-bsxets.xml"
16.11.2017 00:42:27 - "Statistics table: 0 ms"
16.11.2017 00:42:27 - "Test Suite 'Conformance class: Information accessibility, Hydrography' started"
16.11.2017 00:42:27 - "Test Case 'Code lists' started"
16.11.2017 00:42:27 - "Test Assertion 'hy-ia.a.1: Code list extensions accessible': PASSED - 0 ms"
16.11.2017 00:42:27 - "Test Case 'Code lists' finished: PASSED"
16.11.2017 00:42:27 - "Test Case 'Feature references' started"
16.11.2017 00:42:27 - "Test Assertion 'hy-ia.b.1: HydroObject.relatedHydroObject': PASSED - 0 ms"
16.11.2017 00:42:27 - "Test Assertion 'hy-ia.b.2: WatercourseSeparatedCrossing.element': PASSED - 0 ms"
16.11.2017 00:42:27 - "Test Assertion 'hy-ia.b.3: WatercourseLink.startNode': PASSED - 0 ms"
16.11.2017 00:42:27 - "Test Assertion 'hy-ia.b.4: WatercourseLink.endNode': PASSED - 0 ms"
16.11.2017 00:42:27 - "Test Assertion 'hy-ia.b.5: SurfaceWater.bank': PASSED - 0 ms"
16.11.2017 00:42:27 - "Test Assertion 'hy-ia.b.6: SurfaceWater.drainsBasin': PASSED - 0 ms"
16.11.2017 00:42:27 - "Test Assertion 'hy-ia.b.7: SurfaceWater.neighbour': PASSED - 0 ms"
16.11.2017 00:42:27 - "Test Assertion 'hy-ia.b.8: DrainageBasin.outlet': PASSED - 0 ms"
16.11.2017 00:42:27 - "Test Case 'Feature references' finished: PASSED"
16.11.2017 00:42:27 - "Test Suite 'Conformance class: Information accessibility, Hydrography' finished: PASSED"
16.11.2017 00:42:28 - Releasing resources
16.11.2017 00:42:28 - TestRunTask initialized
16.11.2017 00:42:28 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
16.11.2017 00:42:28 - Validation ended with 0 error(s)
16.11.2017 00:42:28 - Compiling test script
16.11.2017 00:42:28 - Starting XQuery tests
16.11.2017 00:42:28 - "Testing 0 features"
16.11.2017 00:42:28 - "Indexing features (parsing errors: 0): 0 ms"
16.11.2017 00:42:28 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data/reference-systems/ets-reference-systems-bsxets.xml"
16.11.2017 00:42:28 - "Statistics table: 1 ms"
16.11.2017 00:42:28 - "Test Suite 'Conformance class: Reference systems, General requirements' started"
16.11.2017 00:42:28 - "Test Case 'Spatial reference systems' started"
16.11.2017 00:42:28 - "Test Assertion 'rs.a.1: Spatial reference systems in feature geometries': PASSED - 0 ms"
16.11.2017 00:42:28 - "Test Assertion 'rs.a.2: Default spatial reference systems in feature collections': PASSED - 0 ms"
16.11.2017 00:42:28 - "Test Case 'Spatial reference systems' finished: PASSED"
16.11.2017 00:42:28 - "Test Case 'Temporal reference systems' started"
16.11.2017 00:42:28 - "Test Assertion 'rs.a.3: Temporal reference systems in features': PASSED - 0 ms"
16.11.2017 00:42:28 - "Test Case 'Temporal reference systems' finished: PASSED"
16.11.2017 00:42:28 - "Test Suite 'Conformance class: Reference systems, General requirements' finished: PASSED"
16.11.2017 00:42:29 - Releasing resources
16.11.2017 00:42:29 - TestRunTask initialized
16.11.2017 00:42:29 - Skipping schema validation because no schema file has been set in the test suite. Data are only checked for well-formedness.
16.11.2017 00:42:29 - Validation ended with 0 error(s)
16.11.2017 00:42:29 - Compiling test script
16.11.2017 00:42:29 - Starting XQuery tests
16.11.2017 00:42:29 - "Testing 0 features"
16.11.2017 00:42:29 - "Executing Test Suite: /home/tomcat/.etf/projects/ets-repository/data-hy/hy-rs/ets-hy-rs-bsxets.xml"
16.11.2017 00:42:29 - "Statistics table: 0 ms"
16.11.2017 00:42:29 - "Test Suite 'Conformance class: Reference systems, Hydrography' started"
16.11.2017 00:42:29 - "Test Case 'Units of measure' started"
16.11.2017 00:42:29 - "Test Assertion 'hy-rs.a.1: WatercourseLink.length': PASSED - 0 ms"
16.11.2017 00:42:29 - "Test Assertion 'hy-rs.a.2: DrainageBasin.area': PASSED - 0 ms"
16.11.2017 00:42:29 - "Test Assertion 'hy-rs.a.3: Falls.length': PASSED - 0 ms"
16.11.2017 00:42:29 - "Test Assertion 'hy-rs.a.4: StandingWater.elevation': PASSED - 0 ms"
16.11.2017 00:42:29 - "Test Assertion 'hy-rs.a.5: StandingWater.meanDepth': PASSED - 0 ms"
16.11.2017 00:42:29 - "Test Assertion 'hy-rs.a.6: StandingWater.surfaceArea': PASSED - 0 ms"
16.11.2017 00:42:29 - "Test Assertion 'hy-rs.a.7: Watercourse.length': PASSED - 0 ms"
16.11.2017 00:42:29 - "Test Assertion 'hy-rs.a.8: Watercourse.width.lower': PASSED - 0 ms"
16.11.2017 00:42:29 - "Test Assertion 'hy-rs.a.9: Watercourse.width.upper': PASSED - 0 ms"
16.11.2017 00:42:29 - "Test Case 'Units of measure' finished: PASSED"
16.11.2017 00:42:29 - "Test Suite 'Conformance class: Reference systems, Hydrography' finished: PASSED"
16.11.2017 00:42:30 - Releasing resources
16.11.2017 00:42:30 - Changed state from INITIALIZED to RUNNING
16.11.2017 00:42:30 - Duration: 17sec
16.11.2017 00:42:30 - TestRun finished
16.11.2017 00:42:30 - Changed state from RUNNING to COMPLETED
