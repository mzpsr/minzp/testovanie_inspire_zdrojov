31.01.2018 00:24:51 - Preparing Test Run sk_testing_inspire_resources_app__undefined__RUN__EIDed2d3501-d700-4ff9-b9bf-070dece8ddbd__2 (initiated Wed Jan 31 00:24:51 CET 2018)
31.01.2018 00:24:51 - Resolving Executable Test Suite dependencies
31.01.2018 00:24:51 - Preparing 2 Test Task:
31.01.2018 00:24:51 -  TestTask 1 (b812b268-6c0a-495c-9dff-6bd783ca7bbd)
31.01.2018 00:24:51 -  will perform tests on Test Object 'INSPIRE Download Service - Cadastral Parcel - KN' by using Executable Test Suite 'WFS 2.0 (OGC 09-025r2/ISO 19142) Conformance Test Suite (EID: 95a1b6fc-2b55-3d43-9502-3b8b605bda10, V: 1.26.0 )'
31.01.2018 00:24:51 -  with parameters: 
31.01.2018 00:24:51 - etf.testcases = *
31.01.2018 00:24:51 -  TestTask 2 (84b2f624-5d82-41d4-b8d7-edd8c83fc992)
31.01.2018 00:24:51 -  will perform tests on Test Object 'INSPIRE Download Service - Cadastral Parcel - KN' by using Executable Test Suite 'Conformance Class: Download Service - Direct WFS (EID: ed2d3501-d700-4ff9-b9bf-070dece8ddbd, V: 1.0.2 )'
31.01.2018 00:24:51 -  with parameters: 
31.01.2018 00:24:51 - etf.testcases = *
31.01.2018 00:24:51 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
31.01.2018 00:24:51 - Setting state to CREATED
31.01.2018 00:24:51 - Changed state from CREATED to INITIALIZING
31.01.2018 00:24:51 - Starting TestRun.18e9b8f2-a0f8-48e3-a08e-2763d187d1e1 at 2018-01-31T00:24:52+01:00
31.01.2018 00:24:52 - Changed state from INITIALIZING to INITIALIZED
31.01.2018 00:24:52 - TestRunTask initialized
31.01.2018 00:24:52 - Invoking TEAM Engine remotely. This may take a while. Progress messages are not supported.
31.01.2018 00:24:52 - Timeout is set to: 20min
31.01.2018 00:29:49 - Results received.
31.01.2018 00:29:51 - Internal ETS model updated.
31.01.2018 00:29:51 - Transforming results.
31.01.2018 00:29:51 - 50 of 5068 assertions passed
31.01.2018 00:29:51 - Releasing resources
31.01.2018 00:29:51 - Project Properties: 
31.01.2018 00:29:51 - etf.testcases - * 
31.01.2018 00:29:51 - serviceEndpoint - https://inspire.skgeodesy.sk/eskn/rest/services/INSPIREWFS/kn_wfs_inspire/GeoDataServer/exts/InspireFeatureDownload/service?ACCEPTVERSIONS=2.0.0&request=GetCapabilities&service=WFS&VERSION=2.0.0 
31.01.2018 00:29:51 - username -  
31.01.2018 00:29:51 - authUser -  
31.01.2018 00:29:51 - authMethod - basic 
31.01.2018 00:29:51 - TestRunTask initialized
31.01.2018 00:30:00 - Releasing resources
31.01.2018 00:30:00 - Changed state from INITIALIZED to RUNNING
31.01.2018 00:30:00 - Duration: 5min
31.01.2018 00:30:00 - TestRun finished
31.01.2018 00:30:00 - Changed state from RUNNING to COMPLETED
