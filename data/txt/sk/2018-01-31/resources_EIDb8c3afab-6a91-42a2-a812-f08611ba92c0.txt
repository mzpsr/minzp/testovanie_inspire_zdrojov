31.01.2018 01:04:31 - Preparing Test Run sk_testing_inspire_resources_app__undefined__RUN__EIDed2d3501-d700-4ff9-b9bf-070dece8ddbd__2 (initiated Wed Jan 31 01:04:31 CET 2018)
31.01.2018 01:04:31 - Resolving Executable Test Suite dependencies
31.01.2018 01:04:31 - Preparing 2 Test Task:
31.01.2018 01:04:31 -  TestTask 1 (4f4232d5-ba3a-4447-9379-053fa8692daf)
31.01.2018 01:04:31 -  will perform tests on Test Object 'INSPIRE Download Service - Cadastral Parcel - UO' by using Executable Test Suite 'WFS 2.0 (OGC 09-025r2/ISO 19142) Conformance Test Suite (EID: 95a1b6fc-2b55-3d43-9502-3b8b605bda10, V: 1.26.0 )'
31.01.2018 01:04:31 -  with parameters: 
31.01.2018 01:04:31 - etf.testcases = *
31.01.2018 01:04:31 -  TestTask 2 (c4bc0a13-7b83-4dcb-8387-f81ba4e14bf1)
31.01.2018 01:04:31 -  will perform tests on Test Object 'INSPIRE Download Service - Cadastral Parcel - UO' by using Executable Test Suite 'Conformance Class: Download Service - Direct WFS (EID: ed2d3501-d700-4ff9-b9bf-070dece8ddbd, V: 1.0.2 )'
31.01.2018 01:04:31 -  with parameters: 
31.01.2018 01:04:31 - etf.testcases = *
31.01.2018 01:04:31 - Test Tasks prepared and ready to be executed. Waiting for the scheduler to start.
31.01.2018 01:04:31 - Setting state to CREATED
31.01.2018 01:04:31 - Changed state from CREATED to INITIALIZING
31.01.2018 01:04:31 - Starting TestRun.b8c3afab-6a91-42a2-a812-f08611ba92c0 at 2018-01-31T01:04:32+01:00
31.01.2018 01:04:32 - Changed state from INITIALIZING to INITIALIZED
31.01.2018 01:04:32 - TestRunTask initialized
31.01.2018 01:04:32 - Invoking TEAM Engine remotely. This may take a while. Progress messages are not supported.
31.01.2018 01:04:32 - Timeout is set to: 20min
31.01.2018 01:17:33 - Results received.
31.01.2018 01:17:34 - Internal ETS model updated.
31.01.2018 01:17:34 - Transforming results.
31.01.2018 01:17:34 - 50 of 5068 assertions passed
31.01.2018 01:17:35 - Releasing resources
31.01.2018 01:17:35 - Project Properties: 
31.01.2018 01:17:35 - etf.testcases - * 
31.01.2018 01:17:35 - serviceEndpoint - https://inspire.skgeodesy.sk/eskn/rest/services/INSPIREWFS/uo_wfs_inspire/GeoDataServer/exts/InspireFeatureDownload/service?ACCEPTVERSIONS=2.0.0&request=GetCapabilities&service=WFS&VERSION=2.0.0 
31.01.2018 01:17:35 - username -  
31.01.2018 01:17:35 - authUser -  
31.01.2018 01:17:35 - authMethod - basic 
31.01.2018 01:17:35 - TestRunTask initialized
31.01.2018 01:18:04 - Releasing resources
31.01.2018 01:18:04 - Changed state from INITIALIZED to RUNNING
31.01.2018 01:18:04 - Duration: 13min
31.01.2018 01:18:04 - TestRun finished
31.01.2018 01:18:04 - Changed state from RUNNING to COMPLETED
