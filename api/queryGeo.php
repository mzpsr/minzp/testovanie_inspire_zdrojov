<?php

$data = file_get_contents("php://input");
$ch = curl_init('http://geoportal.gov.sk/sk/cat-client/search');
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded; charset=UTF-8'));
$result = curl_exec($ch);
$obj = json_decode($result, true);
echo json_encode($obj,JSON_UNESCAPED_SLASHES);

?>