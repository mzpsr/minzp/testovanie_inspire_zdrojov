<?php ?>
<html lang="sk">
 <head>
  <meta charset="UTF-8">
  <title>Validator </title>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <style>
	  div.heading h2
	  {
		color: #999999;
		padding-left: 0;
		/*display: block;*/
		clear: none;
		/*float: left;*/
		font-family: "Droid Sans", sans-serif;
		font-weight: bold;
	  }
	  table, th, td {
		border: 1px solid black;
		border-collapse: collapse;
	}
  </style>
 </head>

 <body>
	<div id="1_serviceStatus" style="display:none; width: 100%">
		<div class="heading">
			<h2>Stav EK JRC validačnej služby</h2>
		</div>
	</div>
	<div id="2_serviceCapab" style="display:none">
		<div class="heading">
			<h2>"Capability" validačnej služby</h2>
		</div>
	</div>
	<div id="3_testObjects" style="display:none">
		<h3>Objekty testovania</h3>
	</div>
	<div id="4_testRuns" style="display:none">
		<h3>Manažment testov</h3>
	</div>
	<div id="5_testResults" style="display:none">
		<h3>Výsledky testov</h3>
	</div>
	<script>
		var url = "https://inspire-sandbox.jrc.ec.europa.eu/etf-webapp";
		var sendData = {};
		$.getJSON('proxy.php?url=' + url + "/v2/status.json", sendData, function (res) {
			$('#1_serviceStatus').show();
			$.each(res.contents, function(k,v){
				//var s = new Date(1331209044000).toISOString();
				$('#1_serviceStatus').append("<p><b>" +k+"</b>: "+v+"</p>");
			})
			
			
		})
		.fail(function() {
			console.log( "error" );
		})
		.always(function() {
			console.log( "complete" );
		  })
		.done(function() {
			$.getJSON('proxy.php?url=' + url + "/v2/ExecutableTestSuites", sendData, function (res){
				$('#2_serviceCapab').append('<div id="2_serviceCapab_1"></div>');
				var selectList;
				selectList += '<select id="ets_list">'
				$.each(res.contents.EtfItemCollection.executableTestSuites.ExecutableTestSuite, function(k,v){
					if (v.disabled === false){
						//console.log("KEY:" ,k);
						//console.log("VALUE:" ,v);
						selectList += '<option id="'+v.id+'" value="'+v.id+'">'+v.label+'</option>';
					}
				})
				selectList += "</select>";
				
				$('#2_serviceCapab_1').append('<h3>Vykonateľné testovacie scenáre (ETS)</h3>');
				$('#2_serviceCapab_1').append(selectList);
				$('#2_serviceCapab').show();
				$('#ets_list').on("change", function(){
					var ets_id = this.value;
					$('#ets_detail').remove();
					var etsDetail = '<div id="ets_detail"><h4>Detaily vybratého ETS</h4><table style="width:100%">';
					etsDetail += '<tr><th>Položka</th><th>Hodnota</th></tr>';
					
					$.getJSON('proxy.php?url=' + url + '/v2/ExecutableTestSuites/' + ets_id, sendData, function(res){
						$.each(res.contents.EtfItemCollection.executableTestSuites.ExecutableTestSuite, function(k,v){
							//var s = new Date(1331209044000).toISOString();
							//console.log(JSON.stringify(v));
							if (typeof v === 'object'){
								$('#ets_detail table').append("<tr><td>" +k+"</td><td>"+JSON.stringify(v)+"</td></tr>");
							}
							else{
								$('#ets_detail table').append("<tr><td>" +k+"</td><td>"+v+"</td></tr>");
							}
						})
					})
					etsDetail += '</table></div>';
					$('#2_serviceCapab_1').append(etsDetail);
					
					
					
					//$('#2_serviceCapab').append('<p>'+ets_id+'</p>');
				})
				
				
			})
		  })
		  .done(function(){
			  $.getJSON('proxy.php?url=' + url + '/v2/TestItemTypes.json', sendData, function(res){
				  $('#2_serviceCapab').append('<div id="2_serviceCapab_2"></div>');
				  var test_item_types = '<div id="test_item_types"><h3>Kroky testov</h3><table style="width:100%">';
				  test_item_types += '<tr><th>Názov</th><th>Popis</th><th>Referencia</th></tr>'
				  $.each(res.contents.EtfItemCollection.testItemTypes.TestItemType, function(k,v){
					//$('#test_item_types table').append("<tr><td>" +v.label+"</td><td>"+v.description+"</td><td>"+v.reference+"</td></tr>");
					test_item_types += ("<tr><td>" +v.label+"</td><td>"+v.description+"</td><td>"+v.reference+"</td></tr>");
				  })
				  test_item_types += '</table></div>';
				  $('#2_serviceCapab_2').append(test_item_types);
			  })
		  })
		  .done(function(){
			  $.getJSON(url + '/v2/Tags.json', sendData, function(res){
				  $('#2_serviceCapab').append('<div id="2_serviceCapab_3"></div>');
				  var tags = '<div id="tags"><h3>Tematicky rozsah testov</h3><table style="width:100%">';
				  tags += '<tr><th>Názov</th><th>Popis</th></tr>'
				  $.each(res.contents.EtfItemCollection.tags.Tag, function(k,v){
					//$('#test_item_types table').append("<tr><td>" +v.label+"</td><td>"+v.description+"</td><td>"+v.reference+"</td></tr>");
					tags += ("<tr><td>" +v.label+"</td><td>"+v.description+"</td></tr>");
				  })
				  tags += '</table></div>';
				  $('#2_serviceCapab_3').append(tags);
			  })
		  })
		  .done(function(){
			  $.getJSON('proxy.php?url=' + url + '/v2/TestObjectTypes.json', sendData, function(res){
				  $('#2_serviceCapab').append('<div id="2_serviceCapab_4"></div>');
				  var object_types = '<div id="object_types"><h3>Objekty testovania</h3><table style="width:100%">';
				  object_types += '<tr><th>Názov</th><th>Popis</th></tr>'
				  $.each(res.contents.EtfItemCollection.testObjectTypes.TestObjectType, function(k,v){
					//$('#test_item_types table').append("<tr><td>" +v.label+"</td><td>"+v.description+"</td><td>"+v.reference+"</td></tr>");
					object_types += ("<tr><td>" +v.label+"</td><td>"+v.description+"</td></tr>");
				  })
				  object_types += '</table></div>';
				  $('#2_serviceCapab_4').append(object_types);
			  })
		  });

		  /*
		  $.getJSON(url + '/v2/TranslationTemplateBundles.json', sendData, function(res){
				$('#3_testObjects').show();
				var trans_bund_temp = '<div id="trans_bund_temp"><h4>Preklady</h4><table style="width:100%">';
				trans_bund_temp += '<tr><th>Jazyk</th><th>Názov položky</th><th>Text</th></tr>';
				$.each(res.EtfItemCollection.translationTemplateBundles.TranslationTemplateBundle, function(k,v){
					$.each(v.translationTemplateCollections.LangTranslationTemplateCollection, function(key,value){
						if (typeof value.translationTemplates != 'undefined' ){
							//console.log(value.translationTemplates.TranslationTemplate.language);
							//console.log(value.translationTemplates.TranslationTemplate.name);
							//console.log(value.translationTemplates.TranslationTemplate.$);
							trans_bund_temp += "<tr><td>" +value.translationTemplates.TranslationTemplate.language+"</td><td>"+value.translationTemplates.TranslationTemplate.name+"</td><td>"+value.translationTemplates.TranslationTemplate.$+"</td></tr>";
							}
						})
					})
					trans_bund_temp += '</table></div>';
					$('#3_testObjects').append(trans_bund_temp);
		  })
			*/
	</script>
 </body>
</html>