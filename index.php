<?php
?>
<!DOCTYPE html>
<html lang="en">
  <head>
	<title>Pilotná aplikácia - Testovanie INSPIRE zdrojov</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/css/bootstrap.min.css" integrity="sha384-/Y6pD6FV/Vv2HJnA6t+vslU6fwYXjCFtcEpHbNJ0lyAFsXTsjBbfaDjzALeQsN6M" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <!--<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>-->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
	<style>
		body {
			padding: 1%;
			height:98%;
			width: 98%;
		}
		button{
			left : 90%;
		}
		#responseData textarea{
			width: 100%;
		}
		/* Style buttons */
			.buttonload {
				background-color: #4CAF50; /* Green background */
				border: none; /* Remove borders */
				color: white; /* White text */
				padding: 12px 16px; /* Some padding */
				font-size: 16px /* Set a font size */
			}
	</style>
  </head>
  <body>
	<div class="dropdown" style="float:right">
	  <button class="btn btn-secondary dropdown-toggle btn-sm" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
		Menu
	  </button>
	  <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
		<a class="dropdown-item" href="http://klimeto.com/projects/2017/mzp/apps/testovanie_inspire_zdrojov/data/html">Reporty</a>
		<a class="dropdown-item" href="http://klimeto.com/projects/2017/mzp/apps/testovanie_inspire_zdrojov/data/txt">Logy</a>
		<a class="dropdown-item" href="http://klimeto.com/projects/2017/mzp/apps/testovanie_inspire_zdrojov/data/xml">Metadáta</a>
		<a class="dropdown-item" href="http://klimeto.com/projects/2017/mzp/apps/testovanie_inspire_zdrojov/html">Scenáre</a>
		<a class="dropdown-item" onclick="ulozZostavu();">Uložiť</a>
	  </div>
	</div>
    <h1><img src="http://klimeto.com/projects/2017/mzp/apps/testovanie_inspire_zdrojov/logo_sk.png" width="100px">SK Testovanie INSPIRE zdrojov (Pilotná aplikácia)</h1>
	<div id="loaderImage" class="float-right" style="display:none;">Nahrávam dáta ...<br><img src="http://klimeto.com/projects/2017/mzp/apps/testovanie_inspire_zdrojov/ajax-loader.gif"></div>
	
	<p>Aplikácia poskytuje spoločné rozhranie pre testovanie INSPIRE zdrojov pomocou <a href="http://klimeto.com/projects/2017/mzp/apps/testovanie_inspire_zdrojov/api/indexETF.php">aplikačno-programového rozhrania</a> (API) <a href="http://inspire.gov.sk/clanky/spolon-inspire-validator" target="_blank">referenčneho INSPIRE validátora</a>.</p>
	<div class="form-group" id="mdHttpUri">
		<label for="mdHttpUriInput">Web adresa INSPIRE metadát (URL):</label>
		<input type="text" class="form-control" id="mdHttpUriInput" placeholder="http://geoportal.gov.sk/sk/cat-client/detail.xml?uuid=https://data.gov.sk/set/rpi/gmd/00156884/f5a95d56-b960-4604-aa27-cf32494ca33b">
	</div>
	<div class="form-group" id="inputValueOptions">
		<label class="radio-inline"><input type="radio" id="optionManual" name="optradio" value="1" checked > Zadať manuálne</label>
		<label class="radio-inline" style="display:none;"><input type="radio" id="optionNG" name="optradio" value="2" > Vyhľadať v Národnom geoportáli</label>
		<label class="radio-inline"><input type="radio" id="optionRPI" name="optradio" value="3"> Vyhľadať v RPI</label>
 		<label class="radio-inline"><input type="radio" id="optionOGC" name="optradio" value="4"> Import z OGC</label>
	</div>
	<div id="searchResults" class="form-group"></div>
	<hr><hr>
	<div id="inspireZdroje" class="form-group">
		<div id="inspireMetadata" style="background-color:#b0e2b4;padding:1%"><span>Metadáta</span></div>
		<div id="inspireServices" style="background-color:#b0e0e2;padding:1%"><span>Sieťové služby</span></div>
		<div id="sdServices" style="background-color:#ccff00;padding:1%"><span>Služby priestorových dát</span></div>
		<div id="inspireData" style="background-color:#e2deb0;padding:1%"><span>Interoperabilita</span></div>
	</div>
	<hr><hr>
	<div id="responseData"></div>
	<div style="float: right; bottom:2.5%; position:relative; text-shadow: 0px 0px 7px rgba(0, 0, 0, 0.75);color: #0080ff; right: 3%;">
		<h5><i>Vyrába </i><a href="http://klimeto.com/" target="_new"><span class="label label-info">KLIMETO</span></a><i> pre </i><a href="http://www.minzp.sk/" target="_new"><span class="label label-info">MŽP</span></a></h5>
		<h5><i>Zdroják na </i><a href="https://gitlab.com/mzpsr/mzpsr/testovanie_inspire_zdrojov" target="_new"><span class="label label-warning">GitLab</span></a></h5>
	</div>
	<script src="http://klimeto.com/projects/2017/mzp/apps/testovanie_inspire_zdrojov/app.js"></script>
	
  </body>
</html>
